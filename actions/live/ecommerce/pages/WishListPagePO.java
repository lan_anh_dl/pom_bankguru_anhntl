package live.ecommerce.pages;

import org.openqa.selenium.WebDriver;
import commons.AbstractPages;
import liveEcommercePages.WishListUI;

public class WishListPagePO extends AbstractPages{
	WebDriver driver;
	public WishListPagePO(WebDriver driver_)
	{
		this.driver =driver_;
	}
	public void ClickShareWishList() {
		waitForControlVisible(driver, WishListUI.SHARE_WISH_LISH_BTN);
		clickToElement(driver, WishListUI.SHARE_WISH_LISH_BTN);
	}
	public boolean isWishListMessDisplay() {
		waitForControlVisible(driver, WishListUI.WISH_LIST_MESS);
		return isControlDisplay(driver, WishListUI.WISH_LIST_MESS);
	}
	public void inputEmail(String value) {
		waitForControlVisible(driver, WishListUI.EMAIL_TXT);
		senkeyToElement(driver, WishListUI.EMAIL_TXT, value);
	}
	public void inputMess(String value) {
		waitForControlVisible(driver, WishListUI.MESS_TXT);
		senkeyToElement(driver, WishListUI.MESS_TXT, value);
	}
	public boolean isWishListMessSucessDisplay() {
		waitForControlVisible(driver, WishListUI.WISH_lIST_MESS_SUSCESS);
		return isControlDisplay(driver, WishListUI.WISH_lIST_MESS_SUSCESS);
	}
	
	public homePagePO clickAddToCard() {
		waitForControlVisible(driver, WishListUI.WISH_LIST_TO_CARD_BTN);
		clickToElement(driver, WishListUI.WISH_LIST_TO_CARD_BTN);
		return PageFactoryManage.getHomePage(driver);
	}
}
