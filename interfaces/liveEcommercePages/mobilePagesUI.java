package liveEcommercePages;

public class mobilePagesUI{
	public static final String PRODUCT_LINK="//h2[@class='product-name']/a[@title='%s']"; 
	public static final String PRICE_SONI="//span[@id='product-price-1']/span";
	public static final String PRICE_IPHONE="//span[@id='product-price-2']/span";
	public static final String ADD_TO_CART="//h2[a[contains(text(),'%s')]]/following-sibling::div[@class='actions']//button";
	public static final String ADD_TO_COMPARE="//h2[a[contains(text(),'%s')]]/following-sibling::div[@class='actions']//a[@class='link-compare']";
	public static final String ADD_TO_WISHLIST="//h2[a[contains(text(),'%s')]]/following-sibling::div[@class='actions']//a[@class='link-wishlist']";

	public static final String DISCOUNT_CODE_TXT="//input [@name='coupon_code']";
	public static final String APPLY_ENTER_BTN="//button[@title='Apply']";
	public static final String DISCOUNT_GENERATED=".//td[contains(text(),'Discount')]//following-sibling::td//following-sibling::span";
	public static final String QTY_TXT="//input[@class='input-text qty']";
	public static final String UPDATE_QTY_BTN="//button[@title='Update']";
	public static final String ERROR_MESS_QTY="//li[@class='error-msg']//span";
	public static final String EMPTY_LINK="//button[@title='Empty Cart']";
	public static final String VERYFI_CART_EMPTY="//div[@class='page-title']/h1";
		public static final String COMPARE_BTN="//button[@title='Compare']";
	public static final String COMPARE_PRODUCT_POPUP="//div[@class='page-title title-buttons']//h1";
	
	
}
