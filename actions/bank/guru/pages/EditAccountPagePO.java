package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.EditAccountPageUI;
import commons.AbstractPages;

public class EditAccountPagePO extends AbstractPages{
	WebDriver driver;
	public EditAccountPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public void inputAccountNo(String value) {
		waitForControlVisible(driver,EditAccountPageUI.ACCOUNT_NO_TXT, value);
		senkeyToElement(driver, EditAccountPageUI.ACCOUNT_NO_TXT, value);
	}
	
	public EditAccountEntryPagePO clickSubmitButton() {
		waitForControlVisible(driver, EditAccountPageUI.SUBMIT_BTN);
		clickToElement(driver, EditAccountPageUI.SUBMIT_BTN);
		return pageFactoryManage.getEditAccountEntryPage(driver);
	}
	public void clickResetButton() {
		waitForControlVisible(driver, EditAccountPageUI.RESET_BTN);
		clickToElement(driver, EditAccountPageUI.RESET_BTN);
		
	}
}
