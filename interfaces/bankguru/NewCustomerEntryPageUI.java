package bankguru;

public class NewCustomerEntryPageUI {
	public static final String CUSTOMER_NAME_TXT="//input[@name='name']";
	public static final String GENDER_RADIO="//input[@name='rad1' and @value='f']";
	public static final String DAYOF_BIRTH="//input[@name='dob']";
	public static final String ADDRESS_TXT ="//textarea[@name='addr']";
	public static final String CITY_TXT="//input[@name='city']";
	public static final String STATE_TXT="//input[@name='state']";

	public static final String PIN_TXT="//input[@name='pinno']";

	public static final String MOBILE_NUMBLE="//input[@name='telephoneno']";

	public static final String EMAIL_TXT="//input[@name='emailid']";
	public static final String PASSWORD="//input[@name='password']";
	public static final String SUBMIT_BTN="//input[@name='sub']";

	public static final String RESET_BTN="//input[@name='res']";
	public static final String NAME_MESSAGE="//label[@id='message']";
	public static final String ADDRESS_MESSAGE="//label[@id='message3']";
	public static final String CITY_MESSAGE="//label[@id='message4']";
	public static final String STATE_MESSAGE="//label[@id='message5']";
	public static final String PIN_MESSAGE=	"//label[@id='message6']";
	public static final String TELEPHONE_MESSAGE="//label[@id='message7']";
	public static final String EMAIL_MESSAGE="//label[@id='message9']";
	//td[contains(text(),'State')]/following-sibling::td
}
