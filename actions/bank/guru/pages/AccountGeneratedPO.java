package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.AbstractsPageUI;
import bankguru.AccountGeneratedUI;
import bankguru.DasboardPageUI;
import commons.AbstractPages;

public class AccountGeneratedPO extends AbstractPages{
	WebDriver driver;
	public AccountGeneratedPO(WebDriver driver)
	{
		this.driver =driver;
	}
	public boolean isAccountGeneratedPageDisplayed() {
		waitForControlVisible(driver, AccountGeneratedUI.CREATED_ACCOUNT_SUCCESS_MESAGE);
		return isControlDisplay(driver, AccountGeneratedUI.CREATED_ACCOUNT_SUCCESS_MESAGE);
	}
public String getValueCurrentAmount()
{waitForControlVisible(driver, AccountGeneratedUI.CURENT_AMOUNT);
	return getTextElement(driver, AccountGeneratedUI.CURENT_AMOUNT);
	}

public String getValueAccountID()
{waitForControlVisible(driver, AccountGeneratedUI.ACCOUNT_ID);
	return getTextElement(driver, AccountGeneratedUI.ACCOUNT_ID);
	}

public HomePagePO clickHomePage() {
waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Manager");
clickToElement(driver,AbstractsPageUI.DYNAMIC_PAGE_LINK,"Manager");
return pageFactoryManage.getHomePagePO(driver);
	}

public DeleteCustomerPagePO clickDeleteCustomerPage() {
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Delete Customer");
clickToElement(driver,AbstractsPageUI.DYNAMIC_PAGE_LINK,"Delete Customer");
return pageFactoryManage.getDeleteCustomerPage(driver);
	}
}
