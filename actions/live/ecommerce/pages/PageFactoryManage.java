package live.ecommerce.pages;

import org.openqa.selenium.WebDriver;

import com.sun.java.swing.plaf.motif.MotifButtonListener;

public class PageFactoryManage {
	 private static SoniXperiaPagePO soniXperiaPage;
		private static IphonePagePO iphonePage;
		private static SamsungPagePO samsungPage;
		private  static homePagePO homePage;
		private  static mobilePagesPO mobilePage;
		private static LoginPagePO loginPage;
		private static DasboardPagePO dasboardPage;
		private static TVPagePO tVPage;
		private static WishListPagePO wishListPage;
		
		public static homePagePO getHomePage(WebDriver driver)
		{
			if(homePage == null)
			{
				return homePage= new homePagePO(driver);
			}
			return homePage;
		}

		public static mobilePagesPO getmobilePage(WebDriver driver)
		{
			if( mobilePage == null)
			{
				return  mobilePage= new mobilePagesPO(driver);
			}
			return  mobilePage;
		}
		
		public static IphonePagePO getIphonePage(WebDriver driver)
		{
			if(iphonePage == null)
			{
				return iphonePage= new IphonePagePO(driver);
			}
			return iphonePage;
		}
		public static SoniXperiaPagePO getSoniXperiaPage(WebDriver driver)
		{
			if(soniXperiaPage == null)
			{
				return soniXperiaPage= new SoniXperiaPagePO(driver);
			}
			return soniXperiaPage;
		}
		public static SamsungPagePO getSamsungPage(WebDriver driver)
		{
			if(samsungPage == null)
			{
				return samsungPage= new SamsungPagePO(driver);
			}
			return samsungPage;
		}
		
		public static LoginPagePO getloginPage(WebDriver driver)
		{
			if(loginPage == null)
			{
				return loginPage= new LoginPagePO(driver);
			}
			return loginPage;
		}
		
		public static DasboardPagePO getDasboardPage(WebDriver driver)
		{
			if(dasboardPage == null)
			{
				return dasboardPage= new DasboardPagePO(driver);
			}
			return dasboardPage;
		}
		
		public static TVPagePO getTVPage(WebDriver driver)
		{
			if( tVPage == null)
			{
				return  tVPage= new TVPagePO(driver);
			}
			return  tVPage;
		}
		public static WishListPagePO getwishListPage(WebDriver driver)
		{
			if( wishListPage == null)
			{
				return  wishListPage= new WishListPagePO(driver);
			}
			return  wishListPage;
		}
}
