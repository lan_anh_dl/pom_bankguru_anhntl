package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.BankCustomerRegistrationPageUI;
import commons.AbstractPages;

public class BankCustomerRegistratioPagePO extends AbstractPages{
	WebDriver driver;
	public BankCustomerRegistratioPagePO(WebDriver driver_)
	{
		this.driver =driver_;
	}
	public boolean addCustomerSucessful() {
		waitForControlVisible(driver, BankCustomerRegistrationPageUI.MESS_SUCCESSFULL);
		return isControlDisplay(driver, BankCustomerRegistrationPageUI.MESS_SUCCESSFULL);
	}
	
	public EditCustomerPagePO clickToEditCustomerLink() {
		waitForControlVisible(driver,BankCustomerRegistrationPageUI.EDIT_CUSTOMER_LINK);
		 clickToElement(driver, BankCustomerRegistrationPageUI.EDIT_CUSTOMER_LINK);
		 return pageFactoryManage.getEditCustomerPagePO(driver);
	}
	public String getCustomerID()
	
	{ waitForControlVisible(driver,BankCustomerRegistrationPageUI.CUSTOMER_ID);
		return getTextElement(driver, BankCustomerRegistrationPageUI.CUSTOMER_ID);
	}

}
