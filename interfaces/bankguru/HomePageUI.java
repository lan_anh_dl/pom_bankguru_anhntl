package bankguru;

public class HomePageUI {

	public static final String WELCOM_MESSAGE="//marquee[text()=\"Welcome To Manager's Page of Guru99 Bank\"]";
	public static final String NEW_CUSTOMER_LINK="//a[contains(text(),'New Customer')]";
	public static final String EDIT_CUSTOMER_LINK="//a[contains(text(),'Edit Customer')]";
	public static final String NEW_ACCOUNT_LINK="//a[contains(text(),'New Account')]"; 
	public static final String DEPOSIT_LINK="//a[contains(text(),'Deposit')]";
	public static final String WITHDRAWAL_LINK="//a[contains(text(),'Withdrawal')]";
	public static final String FUND_TRANSFER_LINK="//a[contains(text(),'Fund Transfer')]";
	public static final String BALANCE_ENQUIRY_LINK="//a[contains(text(),'Balance Enquiry')]";
	public static final String DELETE_ACCOUNT_LINK="//a[contains(text(),'Delete Account')]";
	public static final String DELETE_CUSTOMER_LINK="//a[text()='Delete Customer']";
}
