package bankguru;

public class EditCustomerEntryPageUI {
	public static final String ADRESS_TXT="//textarea[@name='addr']";
	public static final String CITY_TXT="//input[@name='city']";
	public static final String STATE_TXT="//input[@name='state']";
	public static final String PIN_TXT="//input[@name='pinno']";
	public static final String TELEPHONE_TXT="//input[@name='telephoneno']";
	public static final String EMAIL_TXT="//input[@name='emailid']";
	public static final String PASSWORD_TXT="//input[@name='emailid']";
	public static final String SUBMIT_BTN="//input[@name='sub']";
	public static final String RESET_BTN="//input[@name='res']";
	public static final String EDIT_CUSTOMER_PAGE_DISPLAY="//p[contains(text(),'Edit Customer')]" ;
	
	public static final String ADDRESS_MESSAGE="//label[@id='message3']";
	public static final String CITY_MESSAGE="//label[@id='message4']";
	public static final String STATE_MESSAGE="//label[@id='message5']";
	public static final String PIN_MESSAGE=	"//label[@id='message6']";
	public static final String TELEPHONE_MESSAGE="//label[@id='message7']";
	public static final String EMAIL_MESSAGE="//label[@id='message9']";

}
