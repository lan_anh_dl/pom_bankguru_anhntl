package bank.guru.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import bankguru.NewCustomerEntryPageUI;
import commons.AbstractPages;

public class AddNewCustomerEntryPagePO extends AbstractPages{
	WebDriver driver;
	public AddNewCustomerEntryPagePO(WebDriver driver)
	{
		this.driver =driver;
	}

	public void inputCustomerNameTextbox(String username)  {
	waitForControlVisible(driver, NewCustomerEntryPageUI.CUSTOMER_NAME_TXT, username);
	 senkeyToElement(driver, NewCustomerEntryPageUI.CUSTOMER_NAME_TXT, username);
	} 
	public void selectGender() {
		checkTheCheckBox(driver, NewCustomerEntryPageUI.GENDER_RADIO);
	}
	
	public void inputDayOfBirthTextbox(String dayofbirth)  {
		waitForControlVisible(driver, NewCustomerEntryPageUI.DAYOF_BIRTH, dayofbirth);
		WebElement datatimepicker =driver.findElement(By.xpath(NewCustomerEntryPageUI.DAYOF_BIRTH));
		removeAttributeInDOM(driver,datatimepicker ,"type");
		senkeyToElement(driver, NewCustomerEntryPageUI.DAYOF_BIRTH, dayofbirth);
	}
	public void inputCustomerAddressTextbox(String address)  {
	waitForControlVisible(driver, NewCustomerEntryPageUI.ADDRESS_TXT, address);
	 senkeyToElement(driver, NewCustomerEntryPageUI.ADDRESS_TXT, address);
	}
	public void inputCustomerCityTextbox(String city)  {
	waitForControlVisible(driver, NewCustomerEntryPageUI.CITY_TXT, city);
	 senkeyToElement(driver, NewCustomerEntryPageUI.CITY_TXT, city);
	}
	public void inputCustomerStateTextbox(String state)  {
	waitForControlVisible(driver, NewCustomerEntryPageUI.STATE_TXT, state);
	 senkeyToElement(driver, NewCustomerEntryPageUI.STATE_TXT, state);
	}
	public void inputCustomerPinTextbox(String pinno)  {
	waitForControlVisible(driver, NewCustomerEntryPageUI.PIN_TXT, pinno);
	 senkeyToElement(driver, NewCustomerEntryPageUI.PIN_TXT, pinno);
	}
	public void inputCustomerMobiphoneTextbox(String mobiphone)  {
	waitForControlVisible(driver, NewCustomerEntryPageUI.MOBILE_NUMBLE, mobiphone);
	//clearDataElement(driver, NewCustomerEntryPageUI.MOBILE_NUMBLE, mobiphone);
	 senkeyToElement(driver, NewCustomerEntryPageUI.MOBILE_NUMBLE, mobiphone);
	}
	public void inputCustomerEmailTextbox(String email)  {
	waitForControlVisible(driver, NewCustomerEntryPageUI.EMAIL_TXT, email);
	 senkeyToElement(driver, NewCustomerEntryPageUI.EMAIL_TXT, email);
	}
	public void inputCustomerPassWordTextbox(String password)  {
	waitForControlVisible(driver, NewCustomerEntryPageUI.PASSWORD, password);
	 senkeyToElement(driver, NewCustomerEntryPageUI.PASSWORD, password);
	}
	
	public BankCustomerRegistratioPagePO clickToSubmitButton() {
	   waitForControlVisible(driver, NewCustomerEntryPageUI.SUBMIT_BTN);
		clickToElement(driver, NewCustomerEntryPageUI.SUBMIT_BTN);
		return pageFactoryManage.getBankCustomerRegistratioPagePO(driver);
	}
	public void clickToResertButton() {
		waitForControlVisible(driver, NewCustomerEntryPageUI.RESET_BTN);
		clickToElement(driver, NewCustomerEntryPageUI.RESET_BTN);
	}
	public String getErrorMessAdress() {
		waitForControlVisible(driver, NewCustomerEntryPageUI.ADDRESS_MESSAGE);
		return getTextElement(driver, NewCustomerEntryPageUI.ADDRESS_MESSAGE);
		
	}
	public String getErrorMessCity() {
		waitForControlVisible(driver, NewCustomerEntryPageUI.CITY_MESSAGE);
		return getTextElement(driver, NewCustomerEntryPageUI.CITY_MESSAGE);
		
	}
	public String getErrorMessState() {
		waitForControlVisible(driver, NewCustomerEntryPageUI.STATE_MESSAGE);
		return getTextElement(driver, NewCustomerEntryPageUI.STATE_MESSAGE);
		
	}
	public String getErrorMessPin() {
		waitForControlVisible(driver, NewCustomerEntryPageUI.PIN_MESSAGE);
		return getTextElement(driver, NewCustomerEntryPageUI.PIN_MESSAGE);
		
	}
	public String getErrorMessMobile() {
		waitForControlVisible(driver, NewCustomerEntryPageUI.TELEPHONE_MESSAGE);
		return getTextElement(driver, NewCustomerEntryPageUI.TELEPHONE_MESSAGE);
		
	}
	public String getErrorMessMail() {
		waitForControlVisible(driver, NewCustomerEntryPageUI.EMAIL_MESSAGE);
		return getTextElement(driver, NewCustomerEntryPageUI.EMAIL_MESSAGE);
		
	}
	public String getErrorMessName() {
		waitForControlVisible(driver, NewCustomerEntryPageUI.NAME_MESSAGE);
		return getTextElement(driver, NewCustomerEntryPageUI.NAME_MESSAGE);
		
	}

	
}
