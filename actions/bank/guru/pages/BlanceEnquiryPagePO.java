package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.BlanceEnquiryPageUI;

import commons.AbstractPages;

public class BlanceEnquiryPagePO extends AbstractPages{
	WebDriver driver;
	public BlanceEnquiryPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public void inputAccountNo(String value) {
		waitForControlVisible(driver, BlanceEnquiryPageUI.ACCOUNT_NO_TXT ,value);
		senkeyToElement(driver,BlanceEnquiryPageUI.ACCOUNT_NO_TXT , value);
	}
	
	public void clickResetBtn() {
		waitForControlVisible(driver,BlanceEnquiryPageUI.RESET_BTN);
		clickToElement(driver,BlanceEnquiryPageUI.RESET_BTN);
	}
	public BlanceDetailsPagePO clickSubmitBtn() {
		waitForControlVisible(driver,BlanceEnquiryPageUI.SUBMIT_BTN);
		clickToElement(driver, BlanceEnquiryPageUI.SUBMIT_BTN);
		return pageFactoryManage.getBlanceDetailsPage(driver);
		
	}
}
