package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.FundTransferDetailsPageUI;
import commons.AbstractPages;

public class FundTransferDetailsPagePO extends AbstractPages{
	WebDriver driver;
	public FundTransferDetailsPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	
	public boolean FundTransferDetailDisplay() {
		waitForControlVisible(driver, FundTransferDetailsPageUI.FUND_TRANSFER_DETAIL);
		return isControlDisplay(driver, FundTransferDetailsPageUI.FUND_TRANSFER_DETAIL);
		
	}
}
