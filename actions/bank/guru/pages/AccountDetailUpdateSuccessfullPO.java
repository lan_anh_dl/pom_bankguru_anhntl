package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.AccountDetailUpdateSuccessfullUI;
import commons.AbstractPages;

public class AccountDetailUpdateSuccessfullPO extends AbstractPages{
	WebDriver driver;
	public AccountDetailUpdateSuccessfullPO(WebDriver driver)
	{
		this.driver =driver;
	}
	public boolean isAccountDetailUpdatePageDisplayed() {
		waitForControlVisible(driver, AccountDetailUpdateSuccessfullUI.ACC_DETAIL_MESS);
		return isControlDisplay(driver, AccountDetailUpdateSuccessfullUI.ACC_DETAIL_MESS);
	}
}
