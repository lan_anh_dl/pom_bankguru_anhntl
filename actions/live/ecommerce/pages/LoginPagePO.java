package live.ecommerce.pages;

import org.openqa.selenium.WebDriver;

import commons.AbstractPages;
import liveEcommercePages.LoginPageUI;
import liveEcommercePages.homePagesUI;

public class LoginPagePO extends AbstractPages{
	WebDriver driver;
	public LoginPagePO(WebDriver driver)
	{
		this.driver =driver;
	}
	public void inputToEmailTextbox(String email) {
		waitForControlVisible(driver, LoginPageUI.EMAIL_TXT);
		senkeyToElement(driver, LoginPageUI.EMAIL_TXT, email);
		
	}
	public void inputToPasswordTextbox(String pass) {
		waitForControlVisible(driver, LoginPageUI.PASS_TXT, pass);
		senkeyToElement(driver, LoginPageUI.PASS_TXT, pass);
		
	}
	public DasboardPagePO clickToLoginButton() {
		waitForControlVisible(driver,LoginPageUI.lOGIN_SUBMIT_BTN);
		clickToElement(driver, LoginPageUI.lOGIN_SUBMIT_BTN);
		return PageFactoryManage.getDasboardPage(driver);
	}
	
}
