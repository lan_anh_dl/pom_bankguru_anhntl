package bank.guru.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.server.handler.ClearElement;

import bankguru.EditCustomerPageUI;
import bankguru.NewCustomerEntryPageUI;
import commons.AbstractPages;

public class EditCustomerPagePO extends AbstractPages{
	WebDriver driver;
	public EditCustomerPagePO(WebDriver driver_)
	{
		this.driver =driver_;
	}
	public void inputCustomerIDTextbox(String customerID)  {
		waitForControlVisible(driver, EditCustomerPageUI.CUSTOMER_ID_TXT, customerID);
	 senkeyToElement(driver, EditCustomerPageUI.CUSTOMER_ID_TXT, customerID);
	} 
	public String getmessageErrorCustomerID() {
		waitForControlVisible(driver, EditCustomerPageUI.CUSTOMER_ID_MESSAGE);
		return getTextElement(driver, EditCustomerPageUI.CUSTOMER_ID_MESSAGE);
		
	}
	
	public EditCustomerEntryPagePO clickToSubmitButton() {
		waitForControlVisible(driver, EditCustomerPageUI.SUBMIT_CUSTOMER_BTN);
		clickToElement(driver, EditCustomerPageUI.SUBMIT_CUSTOMER_BTN);
		return pageFactoryManage.getEditCustomerEntryPagePO(driver);
	}
	public void clearData() {
		waitForControlVisible(driver, EditCustomerPageUI.CUSTOMER_ID_TXT);
	clearDataElement(driver, EditCustomerPageUI.CUSTOMER_ID_TXT);
	}
	
}
