package live.ecommerce.pages;

import org.openqa.selenium.WebDriver;

import commons.AbstractPages;
import liveEcommercePages.homePagesUI;


public class homePagePO extends AbstractPages{
	WebDriver driver;
	public homePagePO(WebDriver driver_)
	{
		this.driver =driver_;
	}
	public mobilePagesPO clickMobileLink() {
	waitForControlVisible(driver, homePagesUI.MOBILE_LINK);
clickToElement(driver,homePagesUI.MOBILE_LINK);
return PageFactoryManage.getmobilePage(driver);
	}
	
	public LoginPagePO clickMyAccountLink() {
		waitForControlVisible(driver, homePagesUI.MY_ACCOUNT_LINK);
	clickToElement(driver,homePagesUI.MY_ACCOUNT_LINK);
	return PageFactoryManage.getloginPage(driver);
		}
	public void selectCountry(String valueCountry) {
		waitForControlVisible(driver,homePagesUI.COUNTRY_LIST, valueCountry);
		selectItemElemtnInDropDown(driver, homePagesUI.COUNTRY_LIST, valueCountry);
	}
	public void selectState(String valueState) {
		waitForControlVisible(driver,homePagesUI.STATE_LIST,valueState);
				selectItemElemtnInDropDown(driver, homePagesUI.STATE_LIST,valueState);
	}
	public void inputZip(String valueZip) {
		waitForControlVisible(driver, homePagesUI.ZIP_TXT, valueZip);
		senkeyToElement(driver, homePagesUI.ZIP_TXT, valueZip);
	}
	public void clickEstimate() {
		waitForControlVisible(driver, homePagesUI.ESTIMATE_BTN);
		clickToElement(driver, homePagesUI.ESTIMATE_BTN);
	}
	public void checkFlatRate() {
		waitForControlVisible(driver, homePagesUI.FLAT_RATE);
		checkTheCheckBox(driver, homePagesUI.FLAT_RATE);
	}
	public void clickUpdateTotal() {
		waitForControlVisible(driver, homePagesUI.UPDATE_TOTAL_BTN);
		clickToElement(driver, homePagesUI.UPDATE_TOTAL_BTN);
	}
	public String getTextGrandTotal(){
		waitForControlVisible(driver, homePagesUI.GRAND_TOTAL);
		return getTextElement(driver, homePagesUI.GRAND_TOTAL);
	}
	public void clickProcessToCheckOut() {
		waitForControlVisible(driver, homePagesUI.PROCESS_TO_CHECKOUT_BTN);
		clickToElement(driver, homePagesUI.PROCESS_TO_CHECKOUT_BTN);
	}
	public void checkBlingInformation() {
		waitForControlVisible(driver, homePagesUI.BILLING_RADIO);
		checkTheCheckBox(driver, homePagesUI.BILLING_RADIO);
		clickToElement(driver, homePagesUI.CONTINOUN_BTN);
	}
	public void checkShipingInformation() {
		waitForControlVisible(driver, homePagesUI.TEXT_SHIPNG);
		clickToElement(driver, homePagesUI.SHIPING_RADIO);
		clickToElement(driver, homePagesUI.CONTINOUN_SHIPING_BTN);
	}
	public void checkShipingMethod() {
		waitForControlVisible(driver, homePagesUI.TEXT_SHIPING_METHOD);
		clickToElement(driver, homePagesUI.CONTINOUN_SHIPING_METHOD_BTN);
	}
	public void checkPaymentInformation() {
		waitForControlVisible(driver, homePagesUI.CHECK_ODER);
		checkTheCheckBox(driver, homePagesUI.CHECK_ODER);
		clickToElement(driver, homePagesUI.CONTINOUN_PAYMENT_BTN);
	}

	public String getTextGrandTotalReview(){
		waitForControlVisible(driver, homePagesUI.TEXT_ODER_REVIEW);
		return getTextElement(driver, homePagesUI.PRICE_TOTAL);
	}
	
}
