package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.RegisterPageUI;
import commons.AbstractPages;
import bank.guru.pages.pageFactoryManage;

public class RegisterPagePO extends AbstractPages {
	WebDriver driver;
	public RegisterPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public String  getLoginPageUrl()
	{
		return getCurentUrl(driver);
	}
	public void inputToEmailIDTextbox(String email) {
		waitForControlVisible(driver, RegisterPageUI.EMAIL_ID_TXT, email);
		senkeyToElement(driver, RegisterPageUI.EMAIL_ID_TXT, email);
	}
	public void clickToSubmitButton() {
		waitForControlVisible(driver, RegisterPageUI.SUBMIT_BUTTON);
		clickToElement(driver, RegisterPageUI.SUBMIT_BUTTON);
	}
	public String getUserInfor() {
		waitForControlVisible(driver, RegisterPageUI.USER_ID_TEXT);
		return getTextElement(driver, RegisterPageUI.USER_ID_TEXT);
	}
	public String getPasswordInfo() {
		waitForControlVisible(driver, RegisterPageUI.PASSWORD_TEXT);
		return getTextElement(driver, RegisterPageUI.PASSWORD_TEXT);
	}
	public LoginPagePO openLoginPage(String loginPageUrl) 
	{
		openAnyUrl(driver, loginPageUrl);
		return pageFactoryManage.getLoginPage(driver);
	}

}
