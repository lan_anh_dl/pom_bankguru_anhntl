package bankguru;

public class WithdrawalPageUI {
	public static final String ACCOUNT_NO_TXT="//input[@name='accountno']";
	public static final String AMOUNT_TXT="//input[@name='ammount']";
	public static final String DESCRIPTION_WITHDRAWAL_TXT="//input[@name='desc']";
	public static final String SUBMIT_BTN="//input[@name='AccSubmit']";
	public static final String RESET_BTN="//input[@name='res']";

}
