package live.login.validation;

import org.openqa.selenium.WebDriver;

import commons.AbstractPages;
import livegurulogin.LoginPageUI;

public class LoginPagePO extends AbstractPages{
	WebDriver driver;
	public LoginPagePO(WebDriver driver)
	{
		this.driver =driver;
	}
	public void inputToEmailTextbox(String email) {
		//waitForControlVisible(driver, LoginPageUI.EMAIL_TXT);
		senkeyToElement(driver, LoginPageUI.EMAIL_TXT, email);
		
	}
	public void inputToPasswordTextbox(String pass) {
		//waitForControlVisible(driver, LoginPageUI.PASS_TXT);
		senkeyToElement(driver, LoginPageUI.PASS_TXT, pass);
		
	}
	public void clickToLoginButton() {
		//waitForControlVisible(driver, LoginPageUI.lOGIN_SUBMIT_BTN);
		clickToElement(driver, LoginPageUI.lOGIN_SUBMIT_BTN);
		
	}
	
	public String getErrorMessEmailEmpty() {
		//waitForControlVisible(driver, LoginPageUI.EMAIL_EMPTY);
		return getTextElement(driver, LoginPageUI.EMAIL_EMPTY);
		
	}
	public String getErrorMessPassEmpty() {
		//waitForControlVisible(driver, LoginPageUI.PAS_EMPTY);
		return getTextElement(driver, LoginPageUI.PAS_EMPTY);
	}
	public String getErrorMessEmailInvalid() {
		//waitForControlVisible(driver, LoginPageUI.EMAIL_INVALID);
		return getTextElement(driver, LoginPageUI.EMAIL_INVALID);
	}
	public String getErrorMessPassInvalid() {
		//waitForControlVisible(driver, LoginPageUI.PAS_INVALID);
		
		return getTextElement(driver, LoginPageUI.PAS_INVALID);
	}
	public String getErrorMessPassIncorect() {
		//waitForControlVisible(driver, LoginPageUI.PASS_INCORECT);
		
		return getTextElement(driver, LoginPageUI.PASS_INCORECT);
	}
	
}
