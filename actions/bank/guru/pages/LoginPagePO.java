package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import commons.AbstractPages;
import bankguru.LoginPageUI; 
import bank.guru.pages.pageFactoryManage;

public class LoginPagePO extends AbstractPages{
	WebDriver driver;
	public LoginPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public String getLoginPageUrl() {
		return getCurentUrl(driver);
	}
	
	public void inputToUserNameTextbox(String username)  {
	waitForControlVisible(driver, LoginPageUI.USERNAME_TXT, username);
	 senkeyToElement(driver, LoginPageUI.USERNAME_TXT, username);
	}
	
	public void inpuToPasswordTextbox(String password) {
	    waitForControlVisible(driver, LoginPageUI.PASSWORD_TXT, password);
		 senkeyToElement(driver, LoginPageUI.PASSWORD_TXT, password);
	}
	public HomePagePO clickToSubmitButton() {
		waitForControlVisible(driver, LoginPageUI.SUBMIT_BTN);
		clickToElement(driver, LoginPageUI.SUBMIT_BTN);
		return pageFactoryManage.getHomePagePO(driver);
	}
	public void clickToResertButton() {
		waitForControlVisible(driver, LoginPageUI.RESET_BTN);
		clickToElement(driver, LoginPageUI.RESET_BTN);
	}
	public RegisterPagePO clickToHereLink() {
		waitForControlVisible(driver, LoginPageUI.HERE_LINK);
		clickToElement(driver, LoginPageUI.HERE_LINK);
		return pageFactoryManage.getRegisterPagePO(driver);
	}
	

}
