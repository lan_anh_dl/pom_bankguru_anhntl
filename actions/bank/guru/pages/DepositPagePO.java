package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.DepositAmountUI;
import commons.AbstractPages;

public class DepositPagePO extends AbstractPages{
	WebDriver driver;
	public DepositPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public void inputAccountNo(String value) {
		waitForControlVisible(driver, DepositAmountUI.ACCOUNT_NO_TXT,value);
		senkeyToElement(driver, DepositAmountUI.ACCOUNT_NO_TXT, value);
	}
	public void inputAmount(String value) {
		waitForControlVisible(driver, DepositAmountUI.AMOUNT_TXT,value);
		senkeyToElement(driver, DepositAmountUI.AMOUNT_TXT, value);
	}
	public void inputDescription(String value) {
		waitForControlVisible(driver, DepositAmountUI.DESCRIP_TXT,value);
		senkeyToElement(driver, DepositAmountUI.DESCRIP_TXT, value);
	}
	
	public void clickResetBtn(String value) {
		waitForControlVisible(driver, DepositAmountUI.RESET_BTN,value);
		senkeyToElement(driver, DepositAmountUI.RESET_BTN, value);
	}
	public DepositTransactionPO clickSubmitBtn() {
		waitForControlVisible(driver,DepositAmountUI.SUBMIT_BTN);
		clickToElement(driver, DepositAmountUI.SUBMIT_BTN);
		return pageFactoryManage.getDepositTransactionPage(driver);
		
	}
}
