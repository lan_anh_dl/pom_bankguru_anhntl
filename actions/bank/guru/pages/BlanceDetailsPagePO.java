package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.BlanceDetailsPageUI;
import bankguru.DepositTransactionUI;
import commons.AbstractPages;

public class BlanceDetailsPagePO extends AbstractPages{
		WebDriver driver;
		public BlanceDetailsPagePO(WebDriver driver)
		{
			this.driver= driver;
		}
	public boolean BlanceDetailsMess() {
		waitForControlVisible(driver, BlanceDetailsPageUI.BLANCE_DETAIL);
		return isControlDisplay(driver, BlanceDetailsPageUI.BLANCE_DETAIL);
		
	}
	
	public String getBlanceID()
	{
		waitForControlVisible(driver,BlanceDetailsPageUI.BLANCE);
		return getTextElement(driver,BlanceDetailsPageUI.BLANCE);
	}

}
