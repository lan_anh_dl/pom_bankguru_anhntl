package commons;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.oracle.xmlns.internal.webservices.jaxws_databinding.SoapBindingParameterStyle;

import bankguru.AbstractsPageUI;
import live.ecommerce.pages.IphonePagePO;
import live.ecommerce.pages.PageFactoryManage;
import live.ecommerce.pages.SamsungPagePO;
import live.ecommerce.pages.SoniXperiaPagePO;
import live.ecommerce.pages.TVPagePO;
import live.ecommerce.pages.WishListPagePO;
import live.ecommerce.pages.mobilePagesPO;
import liveEcommercePages.mobilePagesUI;
import bank.guru.pages.AddNewCustomerEntryPagePO;
import bank.guru.pages.BlanceEnquiryPagePO;
import bank.guru.pages.DeleteAccountPagePO;
import bank.guru.pages.DeleteCustomerPagePO;
import bank.guru.pages.DepositPagePO;
import bank.guru.pages.EditAccountPagePO;
import bank.guru.pages.FundTransferPagePO;
import bank.guru.pages.HomePagePO;
import bank.guru.pages.NewAcountPagePO;
import bank.guru.pages.WithdrawalPagePO;
import bank.guru.pages.pageFactoryManage;

public class AbstractPages {
	public void openAnyUrl(WebDriver driver, String Url)
	{
		driver.get(Url);
	}
public String getTitle(WebDriver driver)
{
	return driver.getTitle();
	}
public String getCurentUrl(WebDriver driver)
{
	return driver.getCurrentUrl();
	}
public String getPageSource(WebDriver driver)
{
	return driver.getPageSource();
	}

public void back(WebDriver driver)
{
	driver.navigate().back();
	}
public void forWard(WebDriver driver)
{
	driver.navigate().forward();
	}
public void refresh(WebDriver driver)
{
	driver.navigate().refresh();
	}

public void clickToElement(WebDriver driver, String locator)
{
	WebElement element = driver.findElement(By.xpath(locator));
	element.click();
}

public void clickToElement(WebDriver driver, String locator, String value)
{
	locator= String.format(locator, value);
	WebElement element = driver.findElement(By.xpath(locator));
	element.click();
}

public void senkeyToElement(WebDriver driver, String locator, String value)
{
	WebElement element = driver.findElement(By.xpath(locator));
	element.clear();
	element.sendKeys(value);
}

public void  selectItemElemtnInDropDown(WebDriver driver, String locator, String value)
{
	Select selectItem = new Select(driver.findElement(By.xpath(locator)));
	//selectItem.deselectByVisibleText(value);
	selectItem.selectByValue(value);
}


public String getFirstItemSelected(WebDriver driver, String locator)
{
	Select select = new Select(driver.findElement(By.xpath(locator)));
	return select.getFirstSelectedOption().getText();
}

public String getAtribute(WebDriver driver, String locator, String atribute)
{
	WebElement element = driver.findElement(By.xpath(locator));
	return element.getAttribute(atribute);
}

public String getTextElement(WebDriver driver, String locator)
{
	WebElement element = driver.findElement(By.xpath(locator));
	return element.getText();
}
public void clearDataElement(WebDriver driver, String value)
{WebElement element =driver.findElement(By.xpath(value));
element.clear();
	}

public int getSizeElement(WebDriver driver, String locator)
{
	List <WebElement> elements = driver.findElements(By.xpath(locator));
	return elements.size();
}

public void checkTheCheckBox(WebDriver driver, String locator)
{
	WebElement element = driver.findElement(By.xpath(locator));
	if(!element.isSelected())
	{
		element.click();
	}
}


public void unCheckTheCheckBox(WebDriver driver, String locator)
{
	WebElement element = driver.findElement(By.xpath(locator));
	if(element.isSelected())
	{
		element.click();
	}
}

public boolean isControlDisplay(WebDriver driver, String locator)
{
	WebElement element = driver.findElement(By.xpath(locator));
	return element.isDisplayed();
}
public boolean isControlDisplay(WebDriver driver, String locator, String value)
{	locator= String.format(locator, value);
	WebElement element = driver.findElement(By.xpath(locator));
	return element.isDisplayed();
}

public boolean isControlEnable(WebDriver driver, String locator)
{
	WebElement element = driver.findElement(By.xpath(locator));
	return element.isEnabled();
}

public boolean isControlSelected(WebDriver driver, String locator)
{
	WebElement element = driver.findElement(By.xpath(locator));
	return element.isSelected();
}

public void acceptAlert(WebDriver driver)
{
	Alert alert= driver.switchTo().alert();
	 alert.accept();
}

public void canceltAlert(WebDriver driver)
{
	Alert alert= driver.switchTo().alert();
	 alert.dismiss();
}

public String getTextAlert(WebDriver driver)
{
	Alert alert= driver.switchTo().alert();
	return alert.getText();
}

public void getTextAlert(WebDriver driver, String value )
{
	Alert alert= driver.switchTo().alert();
	 alert.sendKeys(value);
}

public void switchToWindowByID(WebDriver driver,String handle ) {
	 handle = driver.getWindowHandle();
	
		driver.switchTo().window(handle);
	
	
}

public void switchToWindowByTitle(WebDriver driver, String expectedTitle) {
	Set<String> allWindows = driver.getWindowHandles();
	for (String runWindow: allWindows) {
	driver.switchTo().window(runWindow);
String actualTittle = driver.getTitle();
if(actualTittle.equals(expectedTitle)){
	break;}
	}}

public void switchToIframe(WebDriver driver,String locator ) {
	 
	
		driver.switchTo().frame(driver.findElement(By.xpath(locator)));
	
	
}
public boolean CloseAllWindowOutparentWindow(WebDriver driver,String parentID) {
	Set<String> allWindows = driver.getWindowHandles();
	for (String runWindow: allWindows)
	{
	 if (!runWindow.equals(parentID))
	{
		driver.switchTo().window(runWindow);
		driver.close();
	}
	}
	
		driver.switchTo().window(parentID);
		if (driver.getWindowHandles().size() == 1) 
			
		return true;
		else
		return false;
}

public void switchToChildwindow(WebDriver driver, String parent) {
	Set<String> allWindows = driver.getWindowHandles();
	for (String runWindow: allWindows)
	{
	 if (runWindow.equals(parent))
	{
		driver.switchTo().window(runWindow);
	break;}
	}}
public void hoverMouse(WebDriver driver,String locator ) {
	 
	WebElement element = driver.findElement(By.xpath(locator));
	Actions action = new Actions(driver);
	action.moveToElement(element).click().build().perform();

}

public void doubleClick(WebDriver driver,String locator ) {
	 
	WebElement element = driver.findElement(By.xpath(locator));
	Actions action = new Actions(driver);
	action.moveToElement(element).doubleClick().build().perform();

}

public void rightClick(WebDriver driver,String locator ) {
	 
	WebElement element = driver.findElement(By.xpath(locator));
	Actions action = new Actions(driver);
	action.moveToElement(element).contextClick().build().perform();

}

public void dragAndDrop(WebDriver driver,String x, String y ) {
	 
	Actions action = new Actions(driver);
	action.dragAndDrop(driver.findElement(By.xpath(x)), driver.findElement(By.xpath(y))).build().perform();

}


public void Upload(WebDriver driver, String locator, String filename)
{
	WebElement fileInput = driver.findElement(By.name(locator));
	fileInput.sendKeys(filename);	
}

public Object executeForBrowserElement(WebDriver driver, String javaSript) {
try {
JavascriptExecutor js = (JavascriptExecutor) driver;
return js.executeScript(javaSript);
} catch (Exception e) {
e.getMessage();
return null;
}
}

public Object executeForWebElement(WebDriver driver, WebElement element) {
try {
JavascriptExecutor js = (JavascriptExecutor) driver;
return js.executeScript("arguments[0].click();", element);
} catch (Exception e) {
e.getMessage();
return null;
}
}
public Object scrollToBottomPage(WebDriver driver, WebElement element) {
try {
JavascriptExecutor js = (JavascriptExecutor) driver;
return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
} catch (Exception e) {
e.getMessage();
return null;
}
}

public void scrollToElement(WebDriver driver, WebElement element )
{
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	

}

public void higtligToElement(WebDriver driver, WebElement element )
{
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].style.border='2px groove green'", element);
}

public void removeAntribute(WebDriver driver, WebElement element )
{
	((JavascriptExecutor) driver).executeScript("arguments[0].removeAttribute('disabled','disabled');", element);
}
public Object removeAttributeInDOM(WebDriver driver, WebElement element,
		String attribute) {
	try {
	JavascriptExecutor js = (JavascriptExecutor) driver;
	return js.executeScript("arguments[0].removeAttribute('" + attribute +
	"');", element);
	} catch (Exception e) {
	e.getMessage();
	return null;
	}
	}


public Boolean checkAnyImagLoaded(WebDriver driver, WebElement image )
{
	JavascriptExecutor js = (JavascriptExecutor) driver;
	return (Boolean) js.executeScript("return arguments[0].complete &&"+" typeof arguments[0].naturalWidth != 'undefined' && arguments[0].naturalWidth > 0", image);

	
}

public void waitForControlPresence(WebDriver driver, String locator)
{
	WebDriverWait wait = new WebDriverWait(driver, 30);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));	
}

public void waitForControlVisible(WebDriver driver, String locator)
{	By by=By.xpath(locator);
	WebDriverWait wait = new WebDriverWait(driver, 30);
	wait.until(ExpectedConditions.visibilityOfElementLocated(by));
}

public void waitForControlVisible(WebDriver driver, String locator, String value)
{	locator= String.format(locator, value);
	By by=By.xpath(locator);
	WebDriverWait wait = new WebDriverWait(driver, 30);
	wait.until(ExpectedConditions.visibilityOfElementLocated(by));
}

public void waitForControlClickable(WebDriver driver, String locator)
{	By by=By.xpath(locator);
	WebDriverWait wait = new WebDriverWait(driver, 30);
	wait.until(ExpectedConditions.elementToBeClickable(by));
	driver.findElement(By.id(locator)).click();
}

public void waitForControlNotVisible(WebDriver driver, String locator)
{	By by=By.xpath(locator);
	new WebDriverWait(driver,
			10).until(ExpectedConditions.invisibilityOfElementLocated(by));
}

public HomePagePO openHomePagePO(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "Manager");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Manager");
	return pageFactoryManage.getHomePagePO(driver);
}

public DeleteAccountPagePO openDeleteAccountPage(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "Delete Account");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Delete Account");
	return pageFactoryManage.getDeleteAccountPage(driver);
}

public DeleteCustomerPagePO openDeleteCustomerPage(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "Delete Customer");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Delete Customer");
	return pageFactoryManage.getDeleteCustomerPage(driver);
}

public DepositPagePO OpenDepositPage(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "Deposit");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Deposit");
	return pageFactoryManage.getDepositPage(driver);
}

public FundTransferPagePO OpenFundTransferPage(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "Fund Transfer");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Fund Transfer");
	return pageFactoryManage.getFundTransferPage(driver);
}
public WithdrawalPagePO OpenWithdrawalPage(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "Withdrawal");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Withdrawal");
	return pageFactoryManage.getWithdrawalPage(driver);
}

public  NewAcountPagePO OpenNewAcountPage(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "New Account");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"New Account");
	return pageFactoryManage.getNewAcountPagePO(driver);
}

public  EditAccountPagePO OpenEditAccountPage(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "Edit Account");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Edit Account");
	return pageFactoryManage.getEditAccountPage(driver);
}
public  BlanceEnquiryPagePO OpenBlanceEnquiryPage(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "Balance Enquiry");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"Balance Enquiry");
	return pageFactoryManage.getBlanceEnquiryPage(driver);
}

public  AddNewCustomerEntryPagePO OpenAddNewCustomerEntryPage(WebDriver driver)
{
	waitForControlVisible(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK, "New Customer");
	clickToElement(driver, AbstractsPageUI.DYNAMIC_PAGE_LINK,"New Customer");
	return pageFactoryManage.getNewCustomerEntryPagePO(driver);
}

public  SamsungPagePO OpenSamsungProduct(WebDriver driver)
{
	waitForControlVisible(driver, mobilePagesUI.PRODUCT_LINK, "Samsung Galaxy");
	clickToElement(driver, mobilePagesUI.PRODUCT_LINK,"Samsung Galaxy");
	return PageFactoryManage.getSamsungPage(driver);
}

public  SoniXperiaPagePO OpenSoniXperiaProduct(WebDriver driver)
{
	waitForControlVisible(driver, mobilePagesUI.PRODUCT_LINK, "Sony Xperia");
	clickToElement(driver, mobilePagesUI.PRODUCT_LINK,"Sony Xperia");
	return PageFactoryManage.getSoniXperiaPage(driver);
}
public  IphonePagePO OpenIphoneProduct(WebDriver driver)
{
	waitForControlVisible(driver, mobilePagesUI.PRODUCT_LINK, "IPhone");
	clickToElement(driver, mobilePagesUI.PRODUCT_LINK,"IPhone");
	return PageFactoryManage.getIphonePage(driver);
}

public mobilePagesPO AddToCartIphone(WebDriver driver) {
	waitForControlVisible(driver,mobilePagesUI.ADD_TO_CART,"IPhone");
	clickToElement(driver, mobilePagesUI.ADD_TO_CART, "IPhone");
	return PageFactoryManage.getmobilePage(driver);
}
public mobilePagesPO AddToCartSoni(WebDriver driver) {
	waitForControlVisible(driver,mobilePagesUI.ADD_TO_CART,"Sony Xperia");
	clickToElement(driver, mobilePagesUI.ADD_TO_CART, "Sony Xperia");
	return PageFactoryManage.getmobilePage(driver);
}

public mobilePagesPO AddToCartSamsung(WebDriver driver) {
	waitForControlVisible(driver,mobilePagesUI.ADD_TO_CART,"Samsung Galaxy");
	clickToElement(driver, mobilePagesUI.ADD_TO_CART, "Samsung Galaxy");
	return PageFactoryManage.getmobilePage(driver);
}
public mobilePagesPO ComparCartSamsung(WebDriver driver) {
	waitForControlVisible(driver,mobilePagesUI.ADD_TO_COMPARE,"Samsung Galaxy");
	clickToElement(driver, mobilePagesUI.ADD_TO_COMPARE, "Samsung Galaxy");
	return PageFactoryManage.getmobilePage(driver);
}
public mobilePagesPO CompareCartIphone(WebDriver driver) {
	waitForControlVisible(driver,mobilePagesUI.ADD_TO_COMPARE,"IPhone");
	clickToElement(driver, mobilePagesUI.ADD_TO_COMPARE, "IPhone");
	return PageFactoryManage.getmobilePage(driver);
}
public mobilePagesPO CompareCartSoni(WebDriver driver) {
	waitForControlVisible(driver,mobilePagesUI.ADD_TO_COMPARE,"Sony Xperia");
	clickToElement(driver, mobilePagesUI.ADD_TO_COMPARE, "Sony Xperia");
	return PageFactoryManage.getmobilePage(driver);
}

public WishListPagePO WishListSamSungLCD(WebDriver driver) {
	waitForControlVisible(driver,mobilePagesUI.ADD_TO_WISHLIST,"Samsung LCD");
	clickToElement(driver, mobilePagesUI.ADD_TO_WISHLIST, "Samsung LCD");
	return PageFactoryManage.getwishListPage(driver);
}
public WishListPagePO WishListLGLCD(WebDriver driver) {
	waitForControlVisible(driver,mobilePagesUI.ADD_TO_WISHLIST,"LG LCD");
	clickToElement(driver, mobilePagesUI.ADD_TO_WISHLIST, "LG LCD");
	return PageFactoryManage.getwishListPage(driver);
}
}
