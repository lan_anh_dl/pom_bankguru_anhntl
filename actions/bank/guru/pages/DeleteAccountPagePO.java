package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.DeleteAccountPageUI;
import bankguru.DeleteCustomerPageUI;
import commons.AbstractPages;

public class DeleteAccountPagePO extends AbstractPages{
	WebDriver driver;
	public DeleteAccountPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	
	public void inputAccountID(String value)
	{waitForControlVisible(driver,DeleteAccountPageUI.ACCOUNT_NO_TXT, value);
		senkeyToElement(driver, DeleteAccountPageUI.ACCOUNT_NO_TXT, value);
	}
	
	public void clickSubmitBtn() {
	waitForControlVisible(driver, DeleteAccountPageUI.SUBMIT_BTN);
	clickToElement(driver,DeleteAccountPageUI.SUBMIT_BTN);
		}
	
	public void clickResetBtn() {
		waitForControlVisible(driver, DeleteAccountPageUI.RESET_BTN);
		clickToElement(driver,DeleteAccountPageUI.RESET_BTN);
			}
	public void  acceptDeleteAccount1() {
		acceptAlert(driver);
		/*String result= getTextAlert(driver);
		acceptAlert(driver);
		return result;*/
			}
	public HomePagePO acceptDeleteAccount2() {
		acceptAlert(driver);
	
		return pageFactoryManage.getHomePagePO(driver);
			}

}
