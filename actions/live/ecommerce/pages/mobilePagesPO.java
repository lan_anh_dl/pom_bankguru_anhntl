package live.ecommerce.pages;

import org.openqa.selenium.WebDriver;

import com.sun.java.swing.plaf.motif.MotifButtonListener;

import commons.AbstractPages;
import liveEcommercePages.mobilePagesUI;

public class mobilePagesPO extends AbstractPages{
	WebDriver driver;
	public mobilePagesPO(WebDriver driver)
	{
		this.driver= driver;
	}
public String getSoniPrice() {
	waitForControlVisible(driver, mobilePagesUI.PRICE_SONI);
	return getTextElement(driver, mobilePagesUI.PRICE_SONI);	
}


public String getIphonePrice() {
	waitForControlVisible(driver, mobilePagesUI.PRICE_IPHONE);
	return getTextElement(driver, mobilePagesUI.PRICE_IPHONE);	
}

public void EnterCouponCode(String value) {
	waitForControlVisible(driver, mobilePagesUI.DISCOUNT_CODE_TXT,value);
	senkeyToElement(driver, mobilePagesUI.DISCOUNT_CODE_TXT, value);
	clickToElement(driver, mobilePagesUI.APPLY_ENTER_BTN);
}
public String getDiscountGenerated() {
	waitForControlVisible(driver, mobilePagesUI.DISCOUNT_GENERATED);
	return getTextElement(driver, mobilePagesUI.DISCOUNT_GENERATED);
}


public void inputChangeQTY(String value) {
	waitForControlVisible(driver, mobilePagesUI.QTY_TXT,value);
	senkeyToElement(driver, mobilePagesUI.QTY_TXT,value);
	clickToElement(driver, mobilePagesUI.UPDATE_QTY_BTN);
}
public void clickEmptyCart() {
	waitForControlVisible(driver, mobilePagesUI.EMPTY_LINK);
	clickToElement(driver, mobilePagesUI.EMPTY_LINK);
}
public String veryfiErrorQTYMess() {
	waitForControlVisible(driver, mobilePagesUI.ERROR_MESS_QTY);
	return getTextElement(driver, mobilePagesUI.ERROR_MESS_QTY);
}

public String veryfiEmtyCart() {
	waitForControlVisible(driver, mobilePagesUI.VERYFI_CART_EMPTY);
	return getTextElement(driver, mobilePagesUI.VERYFI_CART_EMPTY);
}

public void ClickButtonCompare() {
	waitForControlVisible(driver, mobilePagesUI.COMPARE_BTN);
	clickToElement(driver, mobilePagesUI.COMPARE_BTN);
}
public String WindowPopupCompare(){
	//waitForControlVisible(driver,mobilePagesUI.COMPARE_PRODUCT_POPUP);
	return driver.getTitle();
}
}
