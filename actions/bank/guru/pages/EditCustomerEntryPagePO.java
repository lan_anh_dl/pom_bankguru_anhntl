package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.EditCustomerEntryPageUI;
import bankguru.NewCustomerEntryPageUI;
import commons.AbstractPages;

public class EditCustomerEntryPagePO extends AbstractPages{
	WebDriver driver;
	public EditCustomerEntryPagePO(WebDriver driver)
	{
		this.driver =driver;
	}
	
	public boolean InputCustomerIDSucessful() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.EDIT_CUSTOMER_PAGE_DISPLAY);
		return isControlDisplay(driver, EditCustomerEntryPageUI.EDIT_CUSTOMER_PAGE_DISPLAY);
	}
	
	public void inputCustomerAddressTextbox(String address)  {
		waitForControlVisible(driver, EditCustomerEntryPageUI.ADRESS_TXT, address);
	 senkeyToElement(driver, EditCustomerEntryPageUI.ADRESS_TXT, address);
	}
	public void inputCustomerCityTextbox(String city)  {
		waitForControlVisible(driver, EditCustomerEntryPageUI.CITY_TXT, city);
	 senkeyToElement(driver, EditCustomerEntryPageUI.CITY_TXT, city);
	}
	public void inputCustomerStateTextbox(String state)  {
		waitForControlVisible(driver, EditCustomerEntryPageUI.STATE_TXT, state);
	 senkeyToElement(driver, EditCustomerEntryPageUI.STATE_TXT, state);
	}
	public void inputCustomerPinTextbox(String pinno)  {
		waitForControlVisible(driver, EditCustomerEntryPageUI.PIN_TXT, pinno);
	 senkeyToElement(driver, EditCustomerEntryPageUI.PIN_TXT, pinno);
	}
	public void inputCustomerMobiphoneTextbox(String mobiphone)  {
		waitForControlVisible(driver, EditCustomerEntryPageUI.TELEPHONE_TXT, mobiphone);
	 senkeyToElement(driver, EditCustomerEntryPageUI.TELEPHONE_TXT, mobiphone);
	}
	public void inputCustomerEmailTextbox(String email)  {
		waitForControlVisible(driver, EditCustomerEntryPageUI.EMAIL_TXT, email);
	 senkeyToElement(driver, EditCustomerEntryPageUI.EMAIL_TXT, email);
	}
	public void inputCustomerPassWordTextbox(String password)  {
		waitForControlVisible(driver, EditCustomerEntryPageUI.PASSWORD_TXT, password);
	 senkeyToElement(driver, EditCustomerEntryPageUI.PASSWORD_TXT, password);
	}
	
	public BankCustomerRegistratioPagePO clickToSubmitButton() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.SUBMIT_BTN);
		clickToElement(driver, EditCustomerEntryPageUI.SUBMIT_BTN);
		return pageFactoryManage.getBankCustomerRegistratioPagePO(driver);
	}
	public void clickToResertButton() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.RESET_BTN);
		clickToElement(driver, EditCustomerEntryPageUI.RESET_BTN);
	}
	public void hoverToSubmitButton() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.RESET_BTN);
		hoverMouse(driver, EditCustomerEntryPageUI.RESET_BTN);
	}
	public String getErrorMessAdress() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.ADDRESS_MESSAGE);
		return getTextElement(driver, EditCustomerEntryPageUI.ADDRESS_MESSAGE);
		
	}
	public String getErrorMessCity() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.CITY_MESSAGE);
		return getTextElement(driver, EditCustomerEntryPageUI.CITY_MESSAGE);
		
	}
	public String getErrorMessState() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.STATE_MESSAGE);
		return getTextElement(driver, EditCustomerEntryPageUI.STATE_MESSAGE);
		
	}
	public String getErrorMessPin() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.PIN_MESSAGE);
		return getTextElement(driver, EditCustomerEntryPageUI.PIN_MESSAGE);
		
	}
	public String getErrorMessMobile() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.TELEPHONE_MESSAGE);
		return getTextElement(driver, EditCustomerEntryPageUI.TELEPHONE_MESSAGE);
		
	}
	public String getErrorMessMail() {
		waitForControlVisible(driver, EditCustomerEntryPageUI.EMAIL_MESSAGE);
		return getTextElement(driver, EditCustomerEntryPageUI.EMAIL_MESSAGE);
		
	}
	
}
