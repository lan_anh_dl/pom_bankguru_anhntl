package live.login.validation;

import org.openqa.selenium.WebDriver;

import commons.AbstractPages;
import livegurulogin.DashboadPageUI;

public class DasboardPagePO extends AbstractPages{
	WebDriver driver;
	public DasboardPagePO(WebDriver driver_)
	{
		this.driver =driver_;
	}
	public boolean isDashboardPageDisplayed() {
		//waitForControlVisible(driver, DashboadPageUI.MYDOASHBOARD);
		return isControlDisplay(driver, DashboadPageUI.MYDOASHBOARD);
	}
}
