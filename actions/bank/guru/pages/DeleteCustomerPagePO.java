package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.DeleteCustomerPageUI;
import commons.AbstractPages;

public class DeleteCustomerPagePO extends AbstractPages{
	WebDriver driver;
	public DeleteCustomerPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public void inputCustomerID(String value)
	{waitForControlVisible(driver,DeleteCustomerPageUI.CUSTOMER_ID_TXT, value);
		senkeyToElement(driver, DeleteCustomerPageUI.CUSTOMER_ID_TXT, value);
	}
	
	public void clickSubmitBtn() {
	waitForControlVisible(driver, DeleteCustomerPageUI.SUBMIT_BTN);
	clickToElement(driver,DeleteCustomerPageUI.SUBMIT_BTN);
		}
	
	public void clickResetBtn() {
		waitForControlVisible(driver, DeleteCustomerPageUI.RESET_BTN);
		clickToElement(driver,DeleteCustomerPageUI.RESET_BTN);
			}
	public void acceptDelete1() {
		acceptAlert(driver);
			}
	public HomePagePO acceptDelete2() {
		acceptAlert(driver);
	
		return pageFactoryManage.getHomePagePO(driver);
			}

}
