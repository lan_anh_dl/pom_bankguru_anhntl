package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.EditAccountEntryPageUI;
import bankguru.NewAcountPageUI;
import commons.AbstractPages;

public class EditAccountEntryPagePO extends AbstractPages{
	WebDriver driver;
	public EditAccountEntryPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
public void selectTypeOfAccount(String value) {
	waitForControlVisible(driver, EditAccountEntryPageUI.ACOUNT_TYPE_SELECT);
	selectItemElemtnInDropDown(driver, EditAccountEntryPageUI.ACOUNT_TYPE_SELECT, value);
}

public AccountDetailUpdateSuccessfullPO clickSubmitBtn() {
	waitForControlVisible(driver, EditAccountEntryPageUI.ACC_SUBMIT_BTN);
clickToElement(driver,EditAccountEntryPageUI.ACC_SUBMIT_BTN);
return pageFactoryManage.getAccountDetailUpdatePage(driver);
	}
}
