package com.bankguru.login;

import org.testng.annotations.Test;

import commons.Abstracstest;

import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Login_01_CreateUserAndLogin extends Abstracstest{
	WebDriver driver;
	String username, pass, urlCurl;
	Random random = new Random();
	 int number=  random.nextInt(1000);
	  @BeforeClass
	  public void beforeClass() {
		  /*System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
			driver = new ChromeDriver();*/
		  driver.get("http://demo.guru99.com/v4/");
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		  driver.manage().window().maximize();
		  Random random = new Random();
		 int number=  random.nextInt(1000);
		  	
		  
	  }
  @Test
  public void TC_01_CreateUser() {
	  urlCurl= driver.getCurrentUrl();
	  driver.findElement(By.xpath("//a[contains(text(),'here')]")).click();
	  driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys("nameni"+number+"@gmail.com");
	  driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
	  
	  Assert.assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Access details to demo site.')]")).isDisplayed());
	  
	  username=  driver.findElement(By.xpath("//td[contains(text(),'User ID :')]/following-sibling::td")).getText();
	 pass= driver.findElement(By.xpath("//td[contains(text(),'Password :')]/following-sibling::td")).getText();
	  
  }
  
  @Test
  public void TC_02_LoginApplication() {
	  driver.get(urlCurl);
	  driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(username);
	  driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pass);
	  driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
	  
	  WebElement messWelcom = driver.findElement(By.xpath("//marquee"));
	  Assert.assertEquals("Welcome To Manager's Page of Guru99 Bank", messWelcom.getText());
	  
	  WebElement containsUserId = driver.findElement(By.xpath("//td[contains(text(),'Manger Id : "+username+"')]"));
	  Assert.assertTrue(containsUserId.isDisplayed());
	  
	  
  }
  


  @AfterClass
  public void afterClass() {
	 
	  
	  
	  driver.quit();
  }

}
