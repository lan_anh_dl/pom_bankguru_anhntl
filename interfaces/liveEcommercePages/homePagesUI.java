package liveEcommercePages;

public class homePagesUI {
	public static final String MOBILE_LINK="//a[@class='level0 ']";
	public static final String MY_ACCOUNT_LINK="//div[@class='footer']//a[contains(text(),'My Account')]"; 
	public static final String COUNTRY_LIST="//select[@id='country']";
	public static final String STATE_LIST="//select[@id='region_id']";
	public static final String 	ZIP_TXT="//input[@id='postcode']";
	public static final String ESTIMATE_BTN="//button[@title='Estimate']";
	public static final String FLAT_RATE="//input[@id='s_method_flatrate_flatrate']";
	public static final String UPDATE_TOTAL_BTN="//button[@title='Update Total']";
	public static final String GRAND_TOTAL="//table[@id='shopping-cart-totals-table']//strong/span";
	public static final String PROCESS_TO_CHECKOUT_BTN="//ul[@class='checkout-types bottom']//button[@title='Proceed to Checkout']";
	public static final String BILLING_RADIO="//input[@id='billing:use_for_shipping_no']";
	public static final String SHIPING_RADIO="//ul[@class='form-list']//input[@id='shipping:same_as_billing']";
	public static final String CHECK_ODER="//input[@id='p_method_checkmo']";
	public static final String CONTINOUN_BTN="//div[@id='billing-buttons-container']/button";
	public static final String ODER_GENERATE="//div[@id='checkout-step-review']";
	public static final String PRICE_TOTAL="//table[@id='checkout-review-table']//strong//span";
	public static final String TEXT_SHIPNG ="//form[@id='co-shipping-form']/ul/li[1]/label";
	public static final String CONTINOUN_SHIPING_BTN="//div[@id='shipping-buttons-container']/button";
	public static final String TEXT_SHIPING_METHOD="//div[@id='checkout-shipping-method-load']/dl/dt";
	public static final String CONTINOUN_SHIPING_METHOD_BTN="//div[@id='shipping-method-buttons-container']/button";
	public static final String CONTINOUN_PAYMENT_BTN="//div[@id='payment-buttons-container']/button";
	public static final String TEXT_ODER_REVIEW="//table[@id='checkout-review-table']//th[text()='Product']";
}
