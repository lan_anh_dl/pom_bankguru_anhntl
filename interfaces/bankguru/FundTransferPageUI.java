package bankguru;

public class FundTransferPageUI {
	public static final String PAYER_ACCOUNT_TXT="//input[@name='payersaccount']";
	public static final String PAYEE_ACCOUNT_TXT="//input[@name='payeeaccount']";
	public static final String AMOUNT_TXT="//input[@name='ammount']";
	public static final String TRANF_DESC="//input[@name='desc']";
	public static final String ACC_SUBMIT_BTN="//input[@name='AccSubmit']";
	public static final String ACC_RESET_BTN="//input[@name='res']";

}
