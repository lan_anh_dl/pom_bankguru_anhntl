package com.bankguru.login;

import org.testng.annotations.Test;

import com.sun.prism.paint.RadialGradient;

import bank.guru.pages.AccountDetailUpdateSuccessfullPO;
import bank.guru.pages.AccountGeneratedPO;
import bank.guru.pages.AddNewCustomerEntryPagePO;
import bank.guru.pages.BankCustomerRegistratioPagePO;
import bank.guru.pages.BlanceDetailsPagePO;
import bank.guru.pages.BlanceEnquiryPagePO;
import bank.guru.pages.DeleteAccountPagePO;
import bank.guru.pages.DeleteCustomerPagePO;
import bank.guru.pages.DepositPagePO;
import bank.guru.pages.DepositTransactionPO;
import bank.guru.pages.EditAccountEntryPagePO;
import bank.guru.pages.EditAccountPagePO;
import bank.guru.pages.EditCustomerEntryPagePO;
import bank.guru.pages.EditCustomerPagePO;
import bank.guru.pages.FundTransferDetailsPagePO;
import bank.guru.pages.FundTransferPagePO;
import bank.guru.pages.HomePagePO;
import bank.guru.pages.LoginPagePO;
import bank.guru.pages.NewAcountPagePO;
import bank.guru.pages.RegisterPagePO;
import bank.guru.pages.WithdrawalPagePO;
import bank.guru.pages.WithdrawalTransactionPagePO;
import bank.guru.pages.pageFactoryManage;
import bankguru.AccountDetailUpdateSuccessfullUI;
import bankguru.BankCustomerRegistrationPageUI;
import bankguru.DeleteAccountPageUI;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import commons.Abstracstest;

public class Login_01_POM_CreateUserAndLogin2 extends Abstracstest{
	WebDriver driver;
	String username, pass, urlCurl,customernamevalid, dayofbirthvalid, addressvalid ,cityvalid, statevalid, pinnovalid, mobiphonevalid, passwordvalid
	,nameBlank,email ="nameni"+randomNumber()+"@gmail.com"
	,addressBlank,cityBlank,stateBlank,pinBlank, mobileBlank,mailBlank, mailvalid
	,nameNumber,nameSpecialCharacter, nameFirstCharacter,addressFirstBlank,cityNumber,citySpecialCharacter,
	cityFirstBlank, stateNumber,stateSpecialCharacter, stateFirstBlank,pinCharacter,
	pin6Digits,pinSpecialCharacter,pinFirstBlank, telephoneFirstCharacter,  telephoneHaveSpace
	,telephonespecialCharacter, emailNotCorrect
	,nameNumberinput,cityNumberinput, stateNumberinput, pinCharacterinput
	,emailNotCorrectinput ,nameSpecialCharacterinput,citySpecialCharacterinput,stateSpecialCharacterinput,pinSpecialCharacterinput,telephonespecialCharacterinput,
	telephoneHaveSpaceinput, pin6Digitsinput,nameFirstCharacterinput,addressFirstBlankinput, cityFirstBlankinput,stateFirstBlankinput, pinFirstBlankinput, telephoneFirstCharacterinput
	,editcustomerIdBlank,customerIdCharacter, customerIdSpecialCharacter,customerIdValid
	,customerIdCharacterinput,customerIdSpecialCharacterinput,customerIdBlankinput
	,mailvalidedit,pinnovalidedit,statevalidedit,cityvalidedit,addressvalidedit,mobiphonevalidedit
	,accountType,initialDeposit,AccountID,accountTypeEdit,
	AmountDeposit,DescriptionDeposit,TransactionID,
	AmountWithdrawal, DescriptionWithdrawal
	,AccountTO, AmountTransfer, DescriptionTransfer
	,BlanceID;
	private LoginPagePO loginPage;
	private RegisterPagePO registerPage;
	private HomePagePO homePage;
	protected BankCustomerRegistratioPagePO addSucessful;
	protected AddNewCustomerEntryPagePO addNewCustomer;
	private EditCustomerPagePO editCustomeriD;
	private EditCustomerEntryPagePO editCustomerEntry;
	private DeleteCustomerPagePO deleteCustomerPage;
	private AccountGeneratedPO accountGenerated;
	private NewAcountPagePO newAcountPage;
	private EditAccountPagePO editAcountPage;
	private EditAccountEntryPagePO editAccountEntryPage;
	private AccountDetailUpdateSuccessfullPO accountDetailUpdate;
	private DepositPagePO depositPage;
	private DepositTransactionPO depositTransaction;
	private WithdrawalPagePO withdrawalPage;
	private WithdrawalTransactionPagePO withdrawalTransactionPage;
	private FundTransferPagePO fundTransferPage;
	private FundTransferDetailsPagePO fundTransferDetailsPage;
	private BlanceDetailsPagePO blanceDetailsPage;
	private BlanceEnquiryPagePO blanceEnquiryPage;
	private DeleteAccountPagePO deleteAccountPage;
	
	
	@Parameters({"browser", "url"})
	  @BeforeTest
	  public void beforeTest(String browser, String url)  {
		customernamevalid="nguyen thi a";
		dayofbirthvalid="10/11/1998";
		addressvalid="239 cach mang thang tam";
		cityvalid="Ho Chi Minh";
		statevalid="tan binh";
		pinnovalid="123456789";
		mobiphonevalid="933828391";
		passwordvalid="123@name";
		mailvalid="katabid"+randomNumber()+"@gmail.com";
		
		addressvalidedit="375 chung cu ha do";
		cityvalidedit="ha noi";
		statevalidedit="go vap";
		pinnovalidedit="98765432";
		mobiphonevalidedit="988290210";
		mailvalidedit="editedhehe"+randomNumber()+"@gmail.com";
		
		nameBlank="Customer name must not be blank";
		addressBlank="Address Field must not be blank";
		cityBlank="City Field must not be blank";
		stateBlank="State must not be blank";
		pinBlank="PIN Code must not be blank";
		mobileBlank="Mobile no must not be blank";
		mailBlank="Email-ID must not be blank";
		
		

nameNumber="Numbers are not allowed";
nameNumberinput="2345";
nameSpecialCharacter="Special characters are not allowed";
nameSpecialCharacterinput="#$@!";
nameFirstCharacter="First character can not have space";
nameFirstCharacterinput="  gia";

addressFirstBlank="First character can not have space";
addressFirstBlankinput="  dinh";
cityNumber="Numbers are not allowed";
cityNumberinput="546721";
citySpecialCharacter="Special characters are not allowed";
citySpecialCharacterinput="#$@!";
cityFirstBlank="First character can not have space";
cityFirstBlankinput="  ban";

stateNumber="Numbers are not allowed";
stateNumberinput="45679";
stateSpecialCharacter="Special characters are not allowed";
stateSpecialCharacterinput="#$@!";
stateFirstBlank="First character can not have space";
stateFirstBlankinput="  be";
pinCharacter="Characters are not allowed";
pinCharacterinput="cheima";

pin6Digits="PIN Code must have 6 Digits";
pin6Digitsinput="255";
pinSpecialCharacter="Special characters are not allowed";
pinSpecialCharacterinput="#$@!";
pinFirstBlank="First character can not have space";
pinFirstBlankinput="  12345";

telephoneFirstCharacter="First character can not have space";
telephoneFirstCharacterinput="  1234";
telephoneHaveSpace="Characters are not allowed";
telephoneHaveSpaceinput=randomNumber()+"  "+randomNumber();
telephonespecialCharacter="Special characters are not allowed";
telephonespecialCharacterinput="#$@!";

emailNotCorrect="Email-ID is not valid";
emailNotCorrectinput="34b"+randomNumber()+"@";

editcustomerIdBlank="First character can not have space";
customerIdBlankinput=" ";
customerIdCharacter="Characters are not allowed";
customerIdSpecialCharacter="Special characters are not allowed";

customerIdCharacterinput="abdehdaicaday";
customerIdSpecialCharacterinput="@#$@";

accountType="Current";
initialDeposit="5000000";
accountTypeEdit="Savings";

AmountDeposit="6000";
DescriptionDeposit="Deposit";

AmountWithdrawal="1500";
DescriptionWithdrawal="Withdrawal";

AccountTO="43335";
AmountTransfer="3290";
DescriptionTransfer="Transfer";

		 driver = multiBrowser(browser, url);
		 loginPage = pageFactoryManage.getLoginPagePO(driver);
		
		 
	 }
  @Test 
  public void TC_01_CreateUser() {
	  urlCurl= loginPage.getLoginPageUrl();
	  registerPage =loginPage.clickToHereLink();
	  registerPage.inputToEmailIDTextbox(email);
	  registerPage.clickToSubmitButton();
	  
	 Assert.assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Access details to demo site.')]")).isDisplayed());
	  
	  username=  registerPage.getUserInfor();
	 pass= registerPage.getPasswordInfo();
	  
  }
  
  @Test
  public void TC_02_LoginApplication() {
	 loginPage = registerPage.openLoginPage(urlCurl);
	  loginPage.inputToUserNameTextbox(username);
	  loginPage.inpuToPasswordTextbox(pass);
	  homePage=  loginPage.clickToSubmitButton();
	  Assert.assertTrue(homePage.welcomMessegeDisplay());
	  
	    
  }
  @Test
  public void TC_03_Add_Empty() throws Exception {
	  addNewCustomer = homePage.OpenAddNewCustomerEntryPage(driver);
	  addNewCustomer.inputCustomerNameTextbox("");
	  
	  addNewCustomer.inputDayOfBirthTextbox(dayofbirthvalid);
	  addNewCustomer.inputCustomerAddressTextbox("");
	  
	  addNewCustomer.inputCustomerCityTextbox("");
	  
	  addNewCustomer.inputCustomerStateTextbox("");
	 
	  addNewCustomer.inputCustomerPinTextbox("");
	 
	  addNewCustomer.inputCustomerMobiphoneTextbox("");
	 
	  addNewCustomer.inputCustomerEmailTextbox("");
	  
	  addNewCustomer.inputCustomerPassWordTextbox(passwordvalid);  
	  Assert.assertEquals(nameBlank,addNewCustomer.getErrorMessName());
	  Assert.assertEquals(addressBlank,addNewCustomer.getErrorMessAdress());
	  Assert.assertEquals(cityBlank,addNewCustomer.getErrorMessCity());
	  Assert.assertEquals(stateBlank,addNewCustomer.getErrorMessState());
	  Assert.assertEquals(pinBlank,addNewCustomer.getErrorMessPin());
	  Assert.assertEquals(mobileBlank,addNewCustomer.getErrorMessMobile());
	  Assert.assertEquals(mailBlank,addNewCustomer.getErrorMessMail());
  }
  
  @Test
  public void TC_04_Add_Number() throws Exception {
	
	  addNewCustomer.inputCustomerNameTextbox(nameNumberinput);
	  addNewCustomer.inputDayOfBirthTextbox(dayofbirthvalid);
	  addNewCustomer.inputCustomerAddressTextbox(addressvalid);
	  addNewCustomer.inputCustomerCityTextbox(cityNumberinput);
	  addNewCustomer.inputCustomerStateTextbox(stateNumberinput);
	  addNewCustomer.inputCustomerPinTextbox(pinCharacterinput);
	  addNewCustomer.inputCustomerMobiphoneTextbox(mobiphonevalid);
	  addNewCustomer.inputCustomerEmailTextbox(mailvalid);
	  addNewCustomer.inputCustomerPassWordTextbox(passwordvalid);
	  Assert.assertEquals(nameNumber,addNewCustomer.getErrorMessName());
	  Assert.assertEquals(cityNumber,addNewCustomer.getErrorMessCity());
	  Assert.assertEquals(stateNumber,addNewCustomer.getErrorMessState());
	  Assert.assertEquals(pinCharacter,addNewCustomer.getErrorMessPin());
	 
	    
  }
  @Test
  public void TC_05_Add_SpecialCharacter() throws Exception {
	
	  addNewCustomer.inputCustomerNameTextbox(nameSpecialCharacterinput);
	  addNewCustomer.inputDayOfBirthTextbox(dayofbirthvalid);
	  addNewCustomer.inputCustomerAddressTextbox(addressvalid);
	  addNewCustomer.inputCustomerCityTextbox(citySpecialCharacterinput);
	  addNewCustomer.inputCustomerStateTextbox(stateSpecialCharacterinput);
	  addNewCustomer.inputCustomerPinTextbox(pinSpecialCharacterinput);
	  addNewCustomer.inputCustomerMobiphoneTextbox(telephonespecialCharacterinput);
	  addNewCustomer.inputCustomerEmailTextbox(mailvalid);
	  addNewCustomer.inputCustomerPassWordTextbox(passwordvalid);
	  Assert.assertEquals(nameSpecialCharacter,addNewCustomer.getErrorMessName());
	  Assert.assertEquals(citySpecialCharacter,addNewCustomer.getErrorMessCity());
	  Assert.assertEquals(stateSpecialCharacter,addNewCustomer.getErrorMessState());
	  Assert.assertEquals(pinSpecialCharacter,addNewCustomer.getErrorMessPin());
	  Assert.assertEquals(telephonespecialCharacter,addNewCustomer.getErrorMessMobile());  
  }
  @Test
  public void TC_06_Add_First_blank() throws Exception {
	
	  addNewCustomer.inputCustomerNameTextbox(nameFirstCharacterinput);
	  addNewCustomer.inputDayOfBirthTextbox(dayofbirthvalid);
	  addNewCustomer.inputCustomerAddressTextbox(addressFirstBlankinput);
	  addNewCustomer.inputCustomerCityTextbox(cityFirstBlankinput);
	  addNewCustomer.inputCustomerStateTextbox(stateFirstBlankinput);
	  addNewCustomer.inputCustomerPinTextbox(pinFirstBlankinput);
	  Assert.assertEquals(pinFirstBlank,addNewCustomer.getErrorMessPin());
	
	  addNewCustomer.inputCustomerPinTextbox(pin6Digitsinput);
	  Assert.assertEquals(pin6Digits,addNewCustomer.getErrorMessPin()); 
	  addNewCustomer.inputCustomerMobiphoneTextbox(telephoneFirstCharacterinput);
	  Assert.assertEquals(telephoneFirstCharacter,addNewCustomer.getErrorMessMobile());  
	
	  addNewCustomer.inputCustomerMobiphoneTextbox(telephoneHaveSpaceinput);
	  Assert.assertEquals(telephoneHaveSpace,addNewCustomer.getErrorMessMobile());  

	  addNewCustomer.inputCustomerEmailTextbox(emailNotCorrectinput);
	  addNewCustomer.inputCustomerPassWordTextbox(passwordvalid);
	  Assert.assertEquals(nameFirstCharacter,addNewCustomer.getErrorMessName());
	  Assert.assertEquals(addressFirstBlank,addNewCustomer.getErrorMessAdress());
	  Assert.assertEquals(stateFirstBlank,addNewCustomer.getErrorMessState());
	 
	  Assert.assertEquals(emailNotCorrect,addNewCustomer.getErrorMessMail());  
	  
  }

  @Test
  public void TC_07_AddCustomersucessful() throws Exception {
	  addNewCustomer = homePage.OpenAddNewCustomerEntryPage(driver);
	  addNewCustomer.inputCustomerNameTextbox(customernamevalid);
	  addNewCustomer.selectGender();
	  addNewCustomer.inputDayOfBirthTextbox(dayofbirthvalid);
	  addNewCustomer.inputCustomerAddressTextbox(addressvalid);
	  addNewCustomer.inputCustomerCityTextbox(cityvalid);
	  addNewCustomer.inputCustomerStateTextbox(statevalid);
	  addNewCustomer.inputCustomerPinTextbox(pinnovalid);
	  addNewCustomer.inputCustomerMobiphoneTextbox(mobiphonevalid);
	  addNewCustomer.inputCustomerEmailTextbox(mailvalid);
	  addNewCustomer.inputCustomerPassWordTextbox(passwordvalid);
	  addSucessful=  addNewCustomer.clickToSubmitButton();
	  Thread.sleep(3000);
	  Assert.assertTrue(addSucessful.addCustomerSucessful());
	  customerIdValid= addSucessful.getCustomerID();
	    
  }
  @Test
  public void TC_08_InputCustomerID() throws Exception  {
	  
	  editCustomeriD = addSucessful.clickToEditCustomerLink() ; 
	  editCustomeriD.inputCustomerIDTextbox(customerIdBlankinput);
	  Assert.assertEquals(editcustomerIdBlank,editCustomeriD.getmessageErrorCustomerID());
	 
  }
  @Test
  public void TC_09_InputCustomerID() throws Exception  {
	 
	 editCustomeriD.inputCustomerIDTextbox(customerIdCharacterinput);
	  Assert.assertEquals(customerIdCharacter,editCustomeriD.getmessageErrorCustomerID());
	 
	 
  }
  @Test
  public void TC_10_InputCustomerID() throws Exception  {
	 
	 editCustomeriD.inputCustomerIDTextbox(customerIdSpecialCharacterinput);
	  Assert.assertEquals(customerIdSpecialCharacter,editCustomeriD.getmessageErrorCustomerID());
	 
  }
  @Test
  public void TC_11_InputCustomerID() throws Exception  {
	  
	 editCustomeriD.inputCustomerIDTextbox(customerIdValid);
	 Thread.sleep(3000);
	  editCustomerEntry=  editCustomeriD.clickToSubmitButton();
	  Assert.assertTrue(editCustomerEntry.InputCustomerIDSucessful());
	    
  }
  

  @Test
  public void TC_12_Edit_Empty() throws Exception {
	  editCustomerEntry.inputCustomerAddressTextbox("");
	  editCustomerEntry.inputCustomerCityTextbox("");
	  editCustomerEntry.inputCustomerStateTextbox("");
	  editCustomerEntry.inputCustomerPinTextbox("");
	  editCustomerEntry.inputCustomerMobiphoneTextbox("");
	  editCustomerEntry.inputCustomerEmailTextbox("");
	  editCustomerEntry.hoverToSubmitButton();
	
	  Assert.assertEquals(addressBlank,editCustomerEntry.getErrorMessAdress());
	  Assert.assertEquals(cityBlank,editCustomerEntry.getErrorMessCity());
	  Assert.assertEquals(stateBlank,editCustomerEntry.getErrorMessState());
	  Assert.assertEquals(pinBlank,editCustomerEntry.getErrorMessPin());
	  Assert.assertEquals(mobileBlank,editCustomerEntry.getErrorMessMobile());
	  Assert.assertEquals(mailBlank,editCustomerEntry.getErrorMessMail());
	  
  }
  
  @Test
  public void TC_13_Edit_Number() {
	  editCustomerEntry.inputCustomerAddressTextbox(addressvalid);
	  editCustomerEntry.inputCustomerCityTextbox(cityNumberinput);
	  editCustomerEntry.inputCustomerStateTextbox(stateNumberinput);
	  editCustomerEntry.inputCustomerPinTextbox(pinCharacterinput);
	  editCustomerEntry.inputCustomerMobiphoneTextbox(mobiphonevalid);
	  editCustomerEntry.inputCustomerEmailTextbox(mailvalid);
	  Assert.assertEquals(cityNumber,editCustomerEntry.getErrorMessCity());
	  Assert.assertEquals(stateNumber,editCustomerEntry.getErrorMessState());
	  Assert.assertEquals(pinCharacter,editCustomerEntry.getErrorMessPin());
	 
	    
  }
  @Test
  public void TC_14_Edit_SpecialCharacter() {
	
	  editCustomerEntry.inputCustomerAddressTextbox(addressvalid);
	  editCustomerEntry.inputCustomerCityTextbox(citySpecialCharacterinput);
	  editCustomerEntry.inputCustomerStateTextbox(stateSpecialCharacterinput);
	  editCustomerEntry.inputCustomerPinTextbox(pinSpecialCharacterinput);
	  editCustomerEntry.inputCustomerMobiphoneTextbox(telephonespecialCharacterinput);
	  editCustomerEntry.inputCustomerEmailTextbox(mailvalid);
	  
	  Assert.assertEquals(citySpecialCharacter,editCustomerEntry.getErrorMessCity());
	  Assert.assertEquals(stateSpecialCharacter,editCustomerEntry.getErrorMessState());
	  Assert.assertEquals(pinSpecialCharacter,editCustomerEntry.getErrorMessPin());
	  Assert.assertEquals(telephonespecialCharacter,editCustomerEntry.getErrorMessMobile());  
  }
  @Test
  public void TC_15_Edit_First_blank()  {
	
	  editCustomerEntry.inputCustomerAddressTextbox(addressFirstBlankinput);
	  editCustomerEntry.inputCustomerCityTextbox(cityFirstBlankinput);
	  editCustomerEntry.inputCustomerStateTextbox(stateFirstBlankinput);
	  editCustomerEntry.inputCustomerPinTextbox(pinFirstBlankinput);
	  Assert.assertEquals(pinFirstBlank,editCustomerEntry.getErrorMessPin());
	 
	  editCustomerEntry.inputCustomerPinTextbox(pin6Digitsinput);
	  Assert.assertEquals(pin6Digits,editCustomerEntry.getErrorMessPin()); 
	  editCustomerEntry.inputCustomerMobiphoneTextbox(telephoneFirstCharacterinput);
	  Assert.assertEquals(telephoneFirstCharacter,editCustomerEntry.getErrorMessMobile());  
	
	  editCustomerEntry.inputCustomerMobiphoneTextbox(telephoneHaveSpaceinput);
	  Assert.assertEquals(telephoneHaveSpace,editCustomerEntry.getErrorMessMobile());  

	  editCustomerEntry.inputCustomerEmailTextbox(emailNotCorrectinput);
	  editCustomerEntry.inputCustomerPassWordTextbox(passwordvalid);
	 
	  Assert.assertEquals(addressFirstBlank,editCustomerEntry.getErrorMessAdress());
	  Assert.assertEquals(stateFirstBlank,editCustomerEntry.getErrorMessState());
 
	  Assert.assertEquals(emailNotCorrect,editCustomerEntry.getErrorMessMail());  
	  
  }

  @Test
  public void TC_16_EditCustomersucessful() throws Exception {
	
	  editCustomerEntry.inputCustomerAddressTextbox(addressvalidedit);
	  editCustomerEntry.inputCustomerCityTextbox(cityvalidedit);
	  editCustomerEntry.inputCustomerStateTextbox(statevalidedit);
	  editCustomerEntry.inputCustomerPinTextbox(pinnovalidedit);
	  editCustomerEntry.inputCustomerMobiphoneTextbox(mobiphonevalidedit);
	  editCustomerEntry.inputCustomerEmailTextbox(mailvalidedit);
	 
	  addSucessful=  editCustomerEntry.clickToSubmitButton();
	    
  }
  
  
  @Test 
  public void TC_17_AddAcount() throws Exception {
 	 newAcountPage=  homePage.OpenNewAcountPage(driver);
 	 newAcountPage.inputCustomerID(customerIdValid);
 	newAcountPage.selectAcountType(accountType);
 	 newAcountPage.inputInitialDeposit(initialDeposit);
 	 accountGenerated= newAcountPage.clickSubmitBtn();
 	 Assert.assertTrue(accountGenerated.isAccountGeneratedPageDisplayed());
 	 
 	 Assert.assertEquals(initialDeposit,accountGenerated.getValueCurrentAmount());
 	AccountID=accountGenerated.getValueAccountID();
 	System.out.println(customerIdValid);
 	System.out.println(AccountID);
 	
  }
  
  
  @Test
  public void TC_18_EditAcount() {
	  
	  editAcountPage= accountGenerated.OpenEditAccountPage(driver);
	  editAcountPage.inputAccountNo(AccountID);
	 editAccountEntryPage= editAcountPage.clickSubmitButton();
	 editAccountEntryPage.selectTypeOfAccount(accountTypeEdit);
	 accountDetailUpdate= editAccountEntryPage.clickSubmitBtn();		
	Assert.assertTrue(accountDetailUpdate.isAccountDetailUpdatePageDisplayed());  
	
  }
  
  
  @Test 
  public void TC_19_Deposit() {
	 depositPage= accountDetailUpdate.OpenDepositPage(driver);
	 depositPage.inputAccountNo(AccountID);
	 depositPage.inputAmount(AmountDeposit);
	 depositPage.inputDescription(DescriptionDeposit);
	 depositTransaction = depositPage.clickSubmitBtn();
	 Assert.assertTrue(depositTransaction.StransactionDetailDisplay());
	 TransactionID= depositTransaction.getTransactionID();
	  
  }
  
  @Test 
  public void TC_20_WithDrawal()  {
	withdrawalPage=  depositTransaction.OpenWithdrawalPage(driver);
	withdrawalPage.inputAccountNo(AccountID);
	withdrawalPage.inputAmount(AmountWithdrawal);
	withdrawalPage.inputDescription(DescriptionWithdrawal);
	withdrawalTransactionPage= withdrawalPage.clickSubmitBtn();
	Assert.assertTrue(withdrawalTransactionPage.StransactionDetailDisplay());
  }
  
  @Test 
  public void TC_21_FundTransfer()  {
	fundTransferPage=  withdrawalTransactionPage.OpenFundTransferPage(driver);
	fundTransferPage.inputAccountNoFrom(AccountID);
	fundTransferPage.inputAccountNoTo(AccountTO);
	fundTransferPage.inputAmount(AmountTransfer);
	fundTransferPage.inputDescription(DescriptionTransfer);
	fundTransferDetailsPage=fundTransferPage.clickSubmitBtn();
	Assert.assertTrue(fundTransferDetailsPage.FundTransferDetailDisplay());
  }
  
  @Test
  public void TC_22_Blance()  {
	blanceEnquiryPage=fundTransferDetailsPage.OpenBlanceEnquiryPage(driver);
	blanceEnquiryPage.inputAccountNo(AccountID);
	blanceDetailsPage =blanceEnquiryPage.clickSubmitBtn();
	Assert.assertTrue(blanceDetailsPage.BlanceDetailsMess());
	BlanceID= blanceDetailsPage.getBlanceID();
	System.out.println(BlanceID);
	  
  }
  
  @Test
  public void TC_23_DeleteAccount() throws Exception {
	  deleteAccountPage = blanceDetailsPage.openDeleteAccountPage(driver);
	  deleteAccountPage.inputAccountID(AccountID);
	  deleteAccountPage.clickSubmitBtn();
	  deleteAccountPage.acceptDeleteAccount1();
	  Thread.sleep(4000);
	 homePage= deleteAccountPage.acceptDeleteAccount2();
	 Thread.sleep(4000);
	 Assert.assertTrue(homePage.welcomMessegeDisplay());
		 
	 }
  @Test
  public void TC_24_DeleteCustomer() throws Exception {
		 deleteCustomerPage= homePage.openDeleteCustomerPage(driver);
		 deleteCustomerPage.inputCustomerID(customerIdValid);
		 deleteCustomerPage.clickSubmitBtn();
		 deleteCustomerPage.acceptDelete1();
		 Thread.sleep(4000);
		 homePage= deleteCustomerPage.acceptDelete2();
		 Thread.sleep(4000);
		 Assert.assertTrue(homePage.welcomMessegeDisplay());
		 
	 }
  
  
  
  @AfterClass
  public void afterClass() { 
	  driver.quit();
  }

}
