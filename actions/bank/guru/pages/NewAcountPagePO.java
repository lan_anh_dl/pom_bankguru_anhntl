package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.NewAcountPageUI;
import commons.AbstractPages;


public class NewAcountPagePO extends AbstractPages{
	WebDriver driver;
	public NewAcountPagePO(WebDriver driver)
	{
		this.driver =driver;
	}
	public void inputCustomerID(String value)
	{waitForControlVisible(driver,NewAcountPageUI.CUSTOMER_ID_TXT, value);
		senkeyToElement(driver, NewAcountPageUI.CUSTOMER_ID_TXT, value);
	}
	public void inputInitialDeposit(String value)
	{waitForControlVisible(driver,NewAcountPageUI.INITIAL_DEPOSIT_TXT, value);
		senkeyToElement(driver, NewAcountPageUI.INITIAL_DEPOSIT_TXT, value);
	}
	public void selectAcountType(String value)
	{waitForControlVisible(driver,NewAcountPageUI.ACOUNT_TYPE_SELECT, value);
		selectItemElemtnInDropDown(driver, NewAcountPageUI.ACOUNT_TYPE_SELECT, value);
	}
	
	public AccountGeneratedPO clickSubmitBtn() {
	waitForControlVisible(driver, NewAcountPageUI.SUBMIT_BTN);
	clickToElement(driver,NewAcountPageUI.SUBMIT_BTN);
	return pageFactoryManage.getAccountGeneratedPO(driver);
		}
}

