package live.ecommerce.pages;

import org.openqa.selenium.WebDriver;

import commons.AbstractPages;
import liveEcommercePages.DashboadPageUI;

public class DasboardPagePO extends AbstractPages{
	WebDriver driver;
	public DasboardPagePO(WebDriver driver_)
	{
		this.driver =driver_;
	}
	public boolean isDashboardPageDisplayed() {
		waitForControlVisible(driver, DashboadPageUI.MYDOASHBOARD);
		return isControlDisplay(driver, DashboadPageUI.MYDOASHBOARD);
	}
	public TVPagePO clickToTVLink() {
		waitForControlVisible(driver, DashboadPageUI.TV_LINK);
		clickToElement(driver, DashboadPageUI.TV_LINK);
		return PageFactoryManage.getTVPage(driver);
	}
	public WishListPagePO clickToMyWishLink() {
		waitForControlVisible(driver, DashboadPageUI.MY_WISH_LIST_LINK);
		clickToElement(driver, DashboadPageUI.MY_WISH_LIST_LINK);
		return PageFactoryManage.getwishListPage(driver);
	}
}
