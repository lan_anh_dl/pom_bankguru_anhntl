package live.login.validation;

import org.openqa.selenium.WebDriver;

import bankguru.LoginPageUI;
import commons.AbstractPages;
import livegurulogin.HomePageUI;

public class HomePagePO extends AbstractPages{
	WebDriver driver;
	public HomePagePO(WebDriver driver_)
	{
		this.driver =driver_;
	}
	public LoginPagePO clickMyAccountLink() {
	//waitForControlVisible(driver, HomePageUI.MY_ACCOUNT_LINK);
clickToElement(driver,HomePageUI.MY_ACCOUNT_LINK);
return PageFactoryManage.getLoginPage(driver);
	}

}
