package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bank.guru.pages.LoginPagePO;
import bank.guru.pages.HomePagePO;


public class pageFactoryManage {
	private static HomePagePO homePage;
	private static LoginPagePO loginPage;
	private static RegisterPagePO registerPage;
	private static AddNewCustomerEntryPagePO newCustomerEntryPage;
	private static BankCustomerRegistratioPagePO bankCustomerRegistratioPagePO;
	private static EditCustomerPagePO editCustomerPagePO;
	private static EditCustomerEntryPagePO editCustomerEntryPagePO;
	//private static LoginPagePO loginPage;
	 private static AccountGeneratedPO  accountGenerated;
	 private static HomePagePO dashboardPage;
	 private static NewAcountPagePO newAcountPage;
	// private static HomePagePO homePage;
	 private static DeleteAccountPagePO deleteAccountPage;
	 private static DeleteCustomerPagePO deleteCustomerPage;
	 private static BlanceEnquiryPagePO blanceEnquiryPage;
	 private static DepositPagePO depositPage;
	 private static FundTransferPagePO fundTransferPage;
	 private static WithdrawalPagePO withdrawalPage;
	 private static EditAccountPagePO editAccountPage;
	 private static EditAccountEntryPagePO editAccountEntryPage;
	 private static AccountDetailUpdateSuccessfullPO accountDetailUpdate;
	 private static DepositTransactionPO depositTransaction;
	 private static WithdrawalTransactionPagePO withdrawalTransactionPage;
	 private static FundTransferDetailsPagePO fundTransferDetailsPage;
	 private static BlanceDetailsPagePO blanceDetailsPage;
	
	public static HomePagePO getHomePagePO(WebDriver driver)
	{
	if (homePage == null)
	{
	return new HomePagePO(driver);}
	return homePage;

	}
	
	public static LoginPagePO getLoginPagePO(WebDriver driver)
	{
	if (loginPage == null)
	{
	return new LoginPagePO(driver);}
	return loginPage;

	}
	
	public static RegisterPagePO getRegisterPagePO(WebDriver driver)
	{
	if (registerPage == null)
	{
	return new RegisterPagePO(driver);}
	return registerPage;

	}
	public static AddNewCustomerEntryPagePO getNewCustomerEntryPagePO(WebDriver driver)
	{
	if (newCustomerEntryPage == null)
	{
	return new AddNewCustomerEntryPagePO(driver);}
	return newCustomerEntryPage;

	}
	
	public static BankCustomerRegistratioPagePO getBankCustomerRegistratioPagePO(WebDriver driver)
	{
	if (bankCustomerRegistratioPagePO == null)
	{
	return new BankCustomerRegistratioPagePO(driver);}
	return bankCustomerRegistratioPagePO;

	}
	public static EditCustomerPagePO getEditCustomerPagePO(WebDriver driver)
	{
	if (editCustomerPagePO == null)
	{
	return new EditCustomerPagePO(driver);}
	return editCustomerPagePO;

	}
	public static EditCustomerEntryPagePO getEditCustomerEntryPagePO(WebDriver driver)
	{
	if (editCustomerEntryPagePO == null)
	{
	return new EditCustomerEntryPagePO(driver);}
	return editCustomerEntryPagePO;

	}
	
	public static LoginPagePO getLoginPage(WebDriver driver)
	{
		if(loginPage == null)
		{
			return loginPage= new LoginPagePO(driver);
		}
		return loginPage;
	}
	public static HomePagePO getDasboardPagePO(WebDriver driver)
	{
		if(dashboardPage == null)
		{
			return dashboardPage= new HomePagePO(driver);
		}
		return dashboardPage;
	}
	
	public static AccountGeneratedPO getAccountGeneratedPO(WebDriver driver)
	{
		if(accountGenerated == null)
		{
			return accountGenerated= new AccountGeneratedPO(driver);
		}
		return accountGenerated;
	}
	public static NewAcountPagePO getNewAcountPagePO(WebDriver driver)
	{
		if(newAcountPage == null)
		{
			return newAcountPage= new NewAcountPagePO(driver);
		}
		return newAcountPage;
	}
	
	
	public static DeleteAccountPagePO getDeleteAccountPage(WebDriver driver)
	{
		if(deleteAccountPage == null)
		{
			return deleteAccountPage= new DeleteAccountPagePO(driver);
		}
		return deleteAccountPage;
	}
	
	public static DeleteCustomerPagePO getDeleteCustomerPage(WebDriver driver)
	{
		if(deleteCustomerPage == null)
		{
			return deleteCustomerPage= new DeleteCustomerPagePO(driver);
		}
		return deleteCustomerPage;
	}
	
	public static BlanceEnquiryPagePO getBlanceEnquiryPage(WebDriver driver)
	{
		if(blanceEnquiryPage == null)
		{
			return blanceEnquiryPage= new BlanceEnquiryPagePO(driver);
		}
		return blanceEnquiryPage;
	}
	
	public static DepositPagePO getDepositPage(WebDriver driver)
	{
		if(depositPage == null)
		{
			return depositPage= new DepositPagePO(driver);
		}
		return depositPage;
	}
	
	public static FundTransferPagePO getFundTransferPage(WebDriver driver)
	{
		if(fundTransferPage == null)
		{
			return fundTransferPage= new FundTransferPagePO(driver);
		}
		return fundTransferPage;
	}
	public static WithdrawalPagePO getWithdrawalPage(WebDriver driver)
	{
		if(withdrawalPage == null)
		{
			return withdrawalPage= new WithdrawalPagePO(driver);
		}
		return withdrawalPage;
	}
	
	public static EditAccountPagePO getEditAccountPage(WebDriver driver)
	{
		if(editAccountPage == null)
		{
			return editAccountPage= new EditAccountPagePO(driver);
		}
		return editAccountPage;
	}
	
	public static EditAccountEntryPagePO getEditAccountEntryPage(WebDriver driver)
	{
		if(editAccountEntryPage == null)
		{
			return editAccountEntryPage= new EditAccountEntryPagePO(driver);
		}
		return editAccountEntryPage;
	}
	
	public static AccountDetailUpdateSuccessfullPO getAccountDetailUpdatePage(WebDriver driver)
	{
		if(accountDetailUpdate == null)
		{
			return accountDetailUpdate= new AccountDetailUpdateSuccessfullPO(driver);
		}
		return accountDetailUpdate;
	}
	
	public static DepositTransactionPO getDepositTransactionPage(WebDriver driver)
	{
		if(depositTransaction == null)
		{
			return depositTransaction= new DepositTransactionPO(driver);
		}
		return depositTransaction;
	}
	
	public static WithdrawalTransactionPagePO getWithdrawalTransactionPage(WebDriver driver)
	{
		if(withdrawalTransactionPage == null)
		{
			return withdrawalTransactionPage= new WithdrawalTransactionPagePO(driver);
		}
		return withdrawalTransactionPage;
	}
	
	public static FundTransferDetailsPagePO getFundTransferDetailsPage(WebDriver driver)
	{
		if(fundTransferDetailsPage == null)
		{
			return fundTransferDetailsPage= new FundTransferDetailsPagePO(driver);
		}
		return fundTransferDetailsPage;
	}
	
	public static BlanceDetailsPagePO getBlanceDetailsPage(WebDriver driver)
	{
		if(blanceDetailsPage == null)
		{
			return blanceDetailsPage= new BlanceDetailsPagePO(driver);
		}
		return blanceDetailsPage;
	}
	
}
