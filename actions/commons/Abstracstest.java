package commons;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import commons.constants;

public class Abstracstest {
	WebDriver driver;
	
	
	
	public WebDriver multiBrowser(String browser, String url) {

		
		/*switch (browser)
		{
		case "firefox":
			//System.setProperty("webdriver.geckodriver.driver", ".\\resources\\geckodriver.exe");
		driver = new FirefoxDriver();
		break;
		case "chrome":
			System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
			driver = new ChromeDriver();
			break;
			
		case "ie":
			System.setProperty("webdriver.ie.driver", ".\\resources\\IEDriverServer.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			// this line of code is to resolve protected mode issue
			// capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
			// true);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

			driver = new InternetExplorerDriver();
			break;
		}*/

		if (browser.equals("firefox"))

		{
			System.setProperty("webdriver.geckodriver.driver", ".\\resources\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		if (browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
			driver = new ChromeDriver();
			
		} else if (browser.equals("ie")) {
			System.setProperty("webdriver.ie.driver", ".\\resources\\IEDriverServer.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			// this line of code is to resolve protected mode issue
			// capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
			// true);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

			driver = new InternetExplorerDriver();
		}
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
return driver;
	}
	
	 public int randomNumber()
		
		{
			Random random = new Random();
			int rdNumber = random.nextInt(1000);
			return rdNumber;
		}
}

