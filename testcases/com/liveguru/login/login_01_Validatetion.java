package com.liveguru.login;

import org.testng.annotations.Test;

import bank.guru.pages.pageFactoryManage;
import commons.Abstracstest;
import live.login.validation.DasboardPagePO;
import live.login.validation.HomePagePO;
import live.login.validation.LoginPagePO;
import live.login.validation.PageFactoryManage;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

public class login_01_Validatetion extends Abstracstest{
  WebDriver driver;
  private String emailInvalid, PassIncorect, emailValid,passInValid, passValid,
  emailPasswordErrorEmpty, emailErrorInvalid, passwordErrorInvalid,passwordErrorIncorrect;
  private LoginPagePO loginPage;
	private DasboardPagePO dashboardPage;
	private HomePagePO homePage;
	
  @Parameters({"browser", "url"})
	  @BeforeTest
	  public void beforeTest(String browser, String url) {
		emailInvalid = "123434234@12312.123123";
		PassIncorect="123456";
		emailValid= "automationvalid@gmail.com";
		passInValid="123";
		passValid="111111" ;
		
		emailPasswordErrorEmpty = "This is a required field.";
		emailErrorInvalid = "Please enter a valid email address. For example johndoe@domain.com.";
		passwordErrorInvalid = "Please enter 6 or more characters without leading or trailing spaces.";
		passwordErrorIncorrect = "Invalid login or password.";

		 driver = multiBrowser(browser, url);
		 homePage = PageFactoryManage.getHomePage(driver);
		 loginPage = homePage.clickMyAccountLink();
		 
	 }

  
  @Test
  public void TC01_EmailAndPassEmpty() {
	  loginPage.inputToEmailTextbox("");
		loginPage.inputToPasswordTextbox("");
		loginPage.clickToLoginButton();
		Assert.assertEquals(emailPasswordErrorEmpty, loginPage.getErrorMessEmailEmpty());

  }
  
  @Test
  public void TC02_EmailInvalid() {
	  loginPage.inputToEmailTextbox(emailInvalid);
		loginPage.inputToPasswordTextbox(passValid);
		loginPage.clickToLoginButton();
		Assert.assertEquals(emailErrorInvalid, loginPage.getErrorMessEmailInvalid());

  } 
  
  @Test
  public void TC03_PassLessThan6Character() {
	  loginPage.inputToEmailTextbox(emailValid);
		loginPage.inputToPasswordTextbox(passInValid);
		loginPage.clickToLoginButton();
		Assert.assertEquals(passwordErrorInvalid, loginPage.getErrorMessPassInvalid());

  }
  
  @Test
  public void TC04_PasIncorrect() {
	  loginPage.inputToEmailTextbox(emailValid);
		loginPage.inputToPasswordTextbox(PassIncorect);
		loginPage.clickToLoginButton();
		Assert.assertEquals(passwordErrorIncorrect, loginPage.getErrorMessPassIncorect());
  }
  
  @Test
  public void TC05_EmailAndPassValid() {
	
			loginPage.inputToEmailTextbox(emailValid);
			loginPage.inputToPasswordTextbox(passValid);
			loginPage.clickToLoginButton();
			dashboardPage = PageFactoryManage.getDasboardPagePO(driver);
			Assert.assertTrue(dashboardPage.isDashboardPageDisplayed());


  }
  
  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
