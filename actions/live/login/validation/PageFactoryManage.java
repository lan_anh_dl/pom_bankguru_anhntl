package live.login.validation;

import org.openqa.selenium.WebDriver;

public class PageFactoryManage {
	 private static LoginPagePO loginPage;
		private static DasboardPagePO dashboardPage;
		private  static HomePagePO homePage;
		
		public static HomePagePO getHomePage(WebDriver driver)
		{
			if(homePage == null)
			{
				return homePage= new HomePagePO(driver);
			}
			return homePage;
		}

		public static LoginPagePO getLoginPage(WebDriver driver)
		{
			if(loginPage == null)
			{
				return loginPage= new LoginPagePO(driver);
			}
			return loginPage;
		}
		public static DasboardPagePO getDasboardPagePO(WebDriver driver)
		{
			if(dashboardPage == null)
			{
				return dashboardPage= new DasboardPagePO(driver);
			}
			return dashboardPage;
		}
}
