package bankguru;

public class DepositTransactionUI {
	public static final String DEPOSIT_TRANSACTION="//p[contains(text(),'Transaction details of Deposit for Account')]";
	public static final String TRANSACTION_ID="//td[contains(text(),'Transaction ID')]/following-sibling::td";
}
