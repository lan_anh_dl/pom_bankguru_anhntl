package com.liveguru.login;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sun.org.apache.bcel.internal.generic.POP;

import commons.Abstracstest;
import live.ecommerce.pages.SoniXperiaPagePO;
import live.ecommerce.pages.TVPagePO;
import live.ecommerce.pages.WishListPagePO;
import live.ecommerce.pages.homePagePO;
import live.ecommerce.pages.mobilePagesPO;
import live.ecommerce.pages.DasboardPagePO;
import live.ecommerce.pages.LoginPagePO;
import live.ecommerce.pages.PageFactoryManage;

public class eCommerce extends Abstracstest{
	  WebDriver driver;
	  private String priceSoniOnMobile, priceSoniOnDetail, couponePrice,couponeCode,cartEmptyMess,
	  errorQTYMess, qty,headingPopup,emailValid,passValid,
	  emailWishLish,
	  messWishLish,priceTotalReview,
	  priceTotal,
	  valueCountry,
	  valueState,
	  valueZip;
	  private mobilePagesPO mobilePages;
	  private SoniXperiaPagePO soniXperiaPage;
	  private homePagePO homePage;
	  private LoginPagePO loginPage;
	  private DasboardPagePO dasboardPage;
	  private TVPagePO tVpage;
	  private WishListPagePO wishListPage;
	  @Parameters({"browser", "url"})
	  @BeforeTest
	  public void beforeTest(String browser, String url) {
		 couponeCode="GURU50";
		 qty="1000";
		 errorQTYMess="Some of the products cannot be ordered in requested quantity.";
		 cartEmptyMess="SHOPPING CART IS EMPTY";
		 headingPopup="Products Comparison List - Magento Commerce";
			emailValid= "automationvalid@gmail.com";
			passValid="111111" ;
			messWishLish="I want to share my Produst";
			emailWishLish="mailforshare@gmail.com";
			valueCountry="US";
			  valueState="43";
			  valueZip="543432";
		 
		 driver = multiBrowser(browser, url);
		 homePage =PageFactoryManage.getHomePage(driver);
	 }
	  
	  @Test
	  public void TC01_CostOfProduct() {
		  mobilePages= homePage.clickMobileLink();
		  priceSoniOnMobile=  mobilePages.getSoniPrice();
		  soniXperiaPage=  mobilePages.OpenSoniXperiaProduct(driver);
		  priceSoniOnDetail=  soniXperiaPage.getSoniPrice();
		  Assert.assertEquals(priceSoniOnMobile, priceSoniOnDetail);
		
	  }
	  @Test
	  public void TC02_DiscountCoupon() throws Exception {
		  mobilePages= homePage.clickMobileLink();
		  mobilePages.AddToCartIphone(driver);
		  mobilePages.EnterCouponCode(couponeCode);
		 couponePrice= mobilePages.getDiscountGenerated();
		  Assert.assertEquals("-$25.00", couponePrice);
	  }
	  @Test
	  public void TC03_AddMoreProduct() {
		  mobilePages= homePage.clickMobileLink();
		  mobilePages.AddToCartSoni(driver);
		  mobilePages.inputChangeQTY(qty);
		  Assert.assertEquals(errorQTYMess, mobilePages.veryfiErrorQTYMess());
		  mobilePages.clickEmptyCart();
		  Assert.assertEquals(cartEmptyMess, mobilePages.veryfiEmtyCart());
	  }
	  @Test
	  public void TC04_CompareTwoProduct() {
		  mobilePages= homePage.clickMobileLink();
		  mobilePages.CompareCartSoni(driver);
		  mobilePages.CompareCartIphone(driver);
		  String parentID = driver.getWindowHandle();
		  mobilePages.ClickButtonCompare();
		  mobilePages.switchToChildwindow(driver, parentID);
		  mobilePages.switchToWindowByTitle(driver, headingPopup);
		  Assert.assertEquals(headingPopup,mobilePages.WindowPopupCompare());
		  mobilePages.CloseAllWindowOutparentWindow(driver, parentID);
	  }
	  @Test
	  public void TC05_CreateAccount() {
		  loginPage= homePage.clickMyAccountLink();
		  loginPage.inputToEmailTextbox(emailValid);
		  loginPage.inputToPasswordTextbox(passValid);
		 dasboardPage= loginPage.clickToLoginButton();
		 Assert.assertTrue(dasboardPage.isDashboardPageDisplayed());
		 tVpage= dasboardPage.clickToTVLink();
		wishListPage= tVpage.WishListSamSungLCD(driver);
		Assert.assertTrue(wishListPage.isWishListMessDisplay());
		wishListPage.ClickShareWishList();
		wishListPage.inputEmail(emailWishLish);
		wishListPage.inputMess(messWishLish);
		wishListPage.ClickShareWishList();
		Assert.assertTrue(wishListPage.isWishListMessSucessDisplay());
		 
		  
	  }
	  @Test
	  public void TC06_Verify_User() throws Exception {
		
		homePage =wishListPage.clickAddToCard();
		homePage.selectCountry(valueCountry);
		homePage.selectState(valueState);
		homePage.inputZip(valueZip);
		homePage.clickEstimate();
		homePage.checkFlatRate();
		homePage.clickUpdateTotal();
		priceTotal= homePage.getTextGrandTotal();
		homePage.clickProcessToCheckOut();
		homePage.checkBlingInformation();
		homePage.checkShipingInformation();
		homePage.checkShipingMethod();
		homePage.checkPaymentInformation();
		priceTotalReview= homePage.getTextGrandTotalReview();
		Assert.assertEquals(priceTotal, priceTotalReview);
		
	  }
	  
	  @AfterTest
	  public void afterTest() {
		  driver.quit();
	  }

	}

