package bankguru;

public class BankCustomerRegistrationPageUI {
	public static final String MESS_SUCCESSFULL="//p[contains(text(),'Customer Registered Successfully!!!')]";
	public static final String CUSTOMER_ID="//td[contains(text(),'Customer ID')]/following-sibling::td";
	public static final String EDIT_CUSTOMER_LINK="//a[contains(text(),'Edit Customer')]";
	public static final String NEW_ACCOUNT_LINK="//a[contains(text(),'New Account')]"; 
	public static final String DEPOSIT_LINK="//a[contains(text(),'Deposit')]";
	public static final String WITHDRAWAL_LINK="//a[contains(text(),'Withdrawal')]";
	public static final String FUND_TRANSFER_LINK="//a[contains(text(),'Fund Transfer')]";
	public static final String BALANCE_ENQUIRY_LINK="//a[contains(text(),'Balance Enquiry')]";
	public static final String DELETE_ACCOUNT_LINK="//a[contains(text(),'Delete Account')]";
	public static final String DELETE_CUSTOMER_LINK="//a[text()='Delete Customer']";
}