package bank.guru.pages;
import commons.AbstractPages;

import org.openqa.selenium.WebDriver;

import bankguru.HomePageUI;

public class HomePagePO extends AbstractPages{
	WebDriver driver;
	public HomePagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public boolean welcomMessegeDisplay( )
	{
		waitForControlVisible(driver, HomePageUI.WELCOM_MESSAGE);
		return isControlDisplay(driver, HomePageUI.WELCOM_MESSAGE);
	}
	 public AddNewCustomerEntryPagePO clickCustomerLink()
	 {waitForControlVisible(driver,HomePageUI.NEW_CUSTOMER_LINK);
		 clickToElement(driver, HomePageUI.NEW_CUSTOMER_LINK);
		 return pageFactoryManage.getNewCustomerEntryPagePO(driver);
	 }

}
