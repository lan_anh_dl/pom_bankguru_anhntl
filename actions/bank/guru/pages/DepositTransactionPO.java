package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.DepositTransactionUI;
import commons.AbstractPages;

public class DepositTransactionPO extends AbstractPages{
	WebDriver driver;
	public DepositTransactionPO(WebDriver driver)
	{
		this.driver= driver;
	}
	public boolean StransactionDetailDisplay() {
		waitForControlVisible(driver, DepositTransactionUI.DEPOSIT_TRANSACTION);
		return isControlDisplay(driver, DepositTransactionUI.DEPOSIT_TRANSACTION);
		
	}
	
	public String getTransactionID()
	{
		waitForControlVisible(driver, DepositTransactionUI.TRANSACTION_ID);
		return getTextElement(driver, DepositTransactionUI.TRANSACTION_ID);
	}
}

