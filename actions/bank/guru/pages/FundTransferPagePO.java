package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.FundTransferPageUI;
import commons.AbstractPages;

public class FundTransferPagePO extends AbstractPages{
	WebDriver driver;
	public FundTransferPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public void inputAccountNoFrom(String value) {
		waitForControlVisible(driver,FundTransferPageUI.PAYER_ACCOUNT_TXT ,value);
		senkeyToElement(driver, FundTransferPageUI.PAYER_ACCOUNT_TXT, value);
	}
	public void inputAccountNoTo(String value) {
		waitForControlVisible(driver,FundTransferPageUI.PAYEE_ACCOUNT_TXT ,value);
		senkeyToElement(driver,FundTransferPageUI.PAYEE_ACCOUNT_TXT , value);
	}
	public void inputAmount(String value) {
		waitForControlVisible(driver,FundTransferPageUI.AMOUNT_TXT ,value);
		senkeyToElement(driver, FundTransferPageUI.AMOUNT_TXT, value);
	}
	public void inputDescription(String value) {
		waitForControlVisible(driver,FundTransferPageUI.TRANF_DESC ,value);
		senkeyToElement(driver,FundTransferPageUI.TRANF_DESC , value);
	}
	
	public void clickResetBtn(String value) {
		waitForControlVisible(driver,FundTransferPageUI.ACC_RESET_BTN, value);
		clickToElement(driver, FundTransferPageUI.ACC_RESET_BTN, value);
	}
	public FundTransferDetailsPagePO clickSubmitBtn() {
		waitForControlVisible(driver,FundTransferPageUI.ACC_SUBMIT_BTN);
		clickToElement(driver, FundTransferPageUI.ACC_SUBMIT_BTN);
		return pageFactoryManage.getFundTransferDetailsPage(driver);
		
	}
}
