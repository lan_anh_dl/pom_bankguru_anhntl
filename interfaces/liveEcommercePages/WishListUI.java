package liveEcommercePages;

public class WishListUI {
	public static final String WISH_LIST_MESS="//h1[contains(text(),'My Wishlist')]";
	public static final String SHARE_WISH_LISH_BTN="//button[@title='Share Wishlist']";
	public static final String EMAIL_TXT="//textarea[@id='email_address']";
	public static final String MESS_TXT="//textarea[@id='message']";
	public static final String WISH_lIST_MESS_SUSCESS="//li[@class='success-msg']//span";
	public static final String WISH_LIST_TO_CARD_BTN="//div[@class='cart-cell']//button[@class='button btn-cart']";

}
