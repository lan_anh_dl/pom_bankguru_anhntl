package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.WithdrawalPageUI;
import commons.AbstractPages;

public class WithdrawalPagePO extends AbstractPages{
	WebDriver driver;
	public WithdrawalPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public void inputAccountNo(String value) {
		waitForControlVisible(driver, WithdrawalPageUI.ACCOUNT_NO_TXT,value);
		senkeyToElement(driver, WithdrawalPageUI.ACCOUNT_NO_TXT, value);
	}
	public void inputAmount(String value) {
		waitForControlVisible(driver, WithdrawalPageUI.AMOUNT_TXT,value);
		senkeyToElement(driver, WithdrawalPageUI.AMOUNT_TXT, value);
	}
	public void inputDescription(String value) {
		waitForControlVisible(driver, WithdrawalPageUI.DESCRIPTION_WITHDRAWAL_TXT,value);
		senkeyToElement(driver, WithdrawalPageUI.DESCRIPTION_WITHDRAWAL_TXT, value);
	}
	
	public void clickResetBtn(String value) {
		waitForControlVisible(driver, WithdrawalPageUI.RESET_BTN,value);
		senkeyToElement(driver, WithdrawalPageUI.RESET_BTN, value);
	}
	public WithdrawalTransactionPagePO clickSubmitBtn() {
		waitForControlVisible(driver,WithdrawalPageUI.SUBMIT_BTN);
		clickToElement(driver, WithdrawalPageUI.SUBMIT_BTN);
		return pageFactoryManage.getWithdrawalTransactionPage(driver);
		
	}
}
