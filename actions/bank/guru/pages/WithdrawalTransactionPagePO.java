package bank.guru.pages;

import org.openqa.selenium.WebDriver;

import bankguru.WithdrawalTransactionPageUI;
import commons.AbstractPages;

public class WithdrawalTransactionPagePO extends AbstractPages{
	WebDriver driver;
	public WithdrawalTransactionPagePO(WebDriver driver)
	{
		this.driver= driver;
	}
	public boolean StransactionDetailDisplay() {
		waitForControlVisible(driver, WithdrawalTransactionPageUI.WITHDRAWAL_TRANSACTION);
		return isControlDisplay(driver, WithdrawalTransactionPageUI.WITHDRAWAL_TRANSACTION);
		
	}
	
}
