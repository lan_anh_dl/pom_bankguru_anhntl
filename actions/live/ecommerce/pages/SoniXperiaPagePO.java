package live.ecommerce.pages;

import org.openqa.selenium.WebDriver;

import commons.AbstractPages;
import liveEcommercePages.SoniPageUI;
import liveEcommercePages.mobilePagesUI;

public class SoniXperiaPagePO extends AbstractPages{
	WebDriver driver;
	public SoniXperiaPagePO(WebDriver driver_)
	{
		this.driver =driver_;
	}
	public String getSoniPrice() {
		waitForControlVisible(driver, SoniPageUI.PRICE_SONI);
		return getTextElement(driver, SoniPageUI.PRICE_SONI);	
	}

}
